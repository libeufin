/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024-2025 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.common

import org.bouncycastle.math.ec.rfc8032.Ed25519

sealed interface IncomingSubject {
    data class Reserve(val reserve_pub: EddsaPublicKey): IncomingSubject
    data class Kyc(val account_pub: EddsaPublicKey): IncomingSubject
    data object AdminBalanceAdjust: IncomingSubject

    val type: IncomingType get() = when (this) {
        is Reserve -> IncomingType.reserve
        is Kyc -> IncomingType.kyc
        AdminBalanceAdjust -> throw IllegalStateException("Admin balance adjust")
    }
    val key: EddsaPublicKey get() = when (this) {
        is Reserve -> this.reserve_pub
        is Kyc -> this.account_pub
        AdminBalanceAdjust -> throw IllegalStateException("Admin balance adjust")
    }
}

/** Base32 quality by proximity to spec and error probability */
private enum class Base32Quality {
    /// Both mixed casing and mixed characters, that's weird
    Mixed,
    /// Standard but use lowercase, maybe the client shown lowercase in the UI
    Standard,
    /// Uppercase but mixed characters, its common when making typos
    Upper,
    /// Both uppercase and use the standard alphabet as it should
    UpperStandard;

    companion object {
        fun measure(s: String): Base32Quality {
            var uppercase = true;
            var standard = true;
            for (char in s) {
                uppercase = uppercase && char.isUpperCase()
                standard = standard && Base32Crockford.ALPHABET.contains(char)
            }
            return if (uppercase && standard) {
                Base32Quality.UpperStandard
            } else if (uppercase && !standard) {
                Base32Quality.Upper
            } else if (!uppercase && standard) {
                Base32Quality.Standard
            } else {
                Base32Quality.Mixed
            }
        }
    }
}

private data class Candidate(val subject: IncomingSubject, val quality: Base32Quality)

private const val ADMIN_BALANCE_ADJUST = "ADMINBALANCEADJUST"
private const val KEY_SIZE = 52;
private const val KYC_SIZE = KEY_SIZE + 3;
private val ALPHA_NUMBERIC_PATTERN = Regex("[0-9a-zA-Z]*")

/**
 * Extract the public key from an unstructured incoming transfer subject.
 *
 * When a user enters the transfer object in an unstructured way, for ex in
 * their banking UI, they may mistakenly enter separators such as ' \n-+' and
 * make typos.
 * To parse them while ignoring user errors, we reconstruct valid keys from key
 * parts, resolving ambiguities where possible.
 **/
fun parseIncomingSubject(subject: String): IncomingSubject {
    /** Parse an incoming subject */
    fun parseSingle(str: String): Candidate? {
        // Check key type
        val (isKyc, raw) = when (str.length) {
            ADMIN_BALANCE_ADJUST.length -> if (str.equals(ADMIN_BALANCE_ADJUST, ignoreCase = true)) {
                return Candidate(IncomingSubject.AdminBalanceAdjust, Base32Quality.UpperStandard)
            } else {
                return null
            }
            KEY_SIZE -> Pair(false, str)
            KYC_SIZE -> if (str.startsWith("KYC")) {
                Pair(true, str.substring(3))
            } else {
                return null
            }
            else -> return null
        }

        // Check key validity
        val key = try {
            EddsaPublicKey(raw)
        } catch (e: Exception) {
            return null
        }
        if (!Ed25519.validatePublicKeyFull(key.raw, 0)) {
            return null
        }

        val quality = Base32Quality.measure(raw);

        val subject = if (isKyc) IncomingSubject.Kyc(key) else IncomingSubject.Reserve(key)
        return Candidate(subject, quality)
    }

    // Find and concatenate valid parts of a keys
    val parts = mutableListOf(0)
    val concatenated = StringBuilder()
    for (match in ALPHA_NUMBERIC_PATTERN.findAll(subject.replace("%20", " "))) {
        concatenated.append(match.value);
        parts.add(concatenated.length);
    }

    // Find best candidates
    var best: Candidate? = null
    // For each part as a starting point
    for ((i, start) in parts.withIndex()) {
        // Use progressively longer concatenation
        for (end in parts.subList(i, parts.size)) {
            val range = start until end
            // Until they are to long to be a key
            if (range.count() > KYC_SIZE) {
                break;
            }
            // Parse the concatenated parts
            val slice = concatenated.substring(range)
            parseSingle(slice)?.let { other ->
                if (best != null) {
                    if (best.subject is IncomingSubject.AdminBalanceAdjust) {
                        if (other.subject !is IncomingSubject.AdminBalanceAdjust) {
                            throw Exception("Found multiple subject kind")
                        }
                    } else if (other.quality > best.quality // We prefer high quality keys
                        || ( // We prefer prefixed keys over reserve keys
                            best.subject.type == IncomingType.reserve &&
                            (other.subject.type == IncomingType.kyc || other.subject.type == IncomingType.wad)
                        ))
                    {
                        best = other
                    } else if (best.subject.key != other.subject.key // If keys are different
                        && best.quality == other.quality // Of same quality
                        && !( // And prefixing is different
                            (best.subject.type == IncomingType.kyc || best.subject.type == IncomingType.wad) &&
                            other.subject.type == IncomingType.reserve
                        ))
                    {
                        throw Exception("Found multiple reserve public key")
                    }
                } else {
                    best = other
                }
            }
        }
    }

    return best?.subject ?: throw Exception("Missing reserve public key")
}

/** Extract the reserve public key from an incoming Taler transaction subject */
fun parseOutgoingSubject(subject: String): Pair<ShortHashCode, ExchangeUrl>  {
    val (wtid, baseUrl) = subject.splitOnce(" ") ?: throw Exception("Malformed outgoing subject")
    return Pair(EddsaPublicKey(wtid), ExchangeUrl(baseUrl))
}