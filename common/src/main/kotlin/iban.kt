/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024-2025 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.common

// Taken from the ISO20022 XSD schema
private val bicRegex = Regex("^[A-Z]{6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3})?$")

fun validateBic(bic: String): Boolean {
    return bicRegex.matches(bic)
}

// Taken from the ISO20022 XSD schema
private val ibanRegex = Regex("^[A-Z]{2}[0-9]{2}[a-zA-Z0-9]{1,30}$")

fun validateIban(iban: String): Boolean {
    return ibanRegex.matches(iban)
}


@JvmInline
value class IBAN private constructor(val value: String) {
    override fun toString(): String = value

    companion object {
        private val SEPARATOR = Regex("[\\ \\-]")

        fun checksum(iban: String): Int = 
            (iban.substring(4 until iban.length).asSequence() + iban.substring(0 until 4).asSequence())
                .fold(0) { acc, char ->
                    var checksum = if (char.isDigit()) {
                        acc * 10 + char.code - '0'.code
                    } else {
                        acc * 100 + char.code - 'A'.code + 10
                    }
                    if (checksum > 9_999_999) {
                        checksum %= 97
                    }
                    checksum
                } % 97
            

        fun parse(raw: String): IBAN {
            val iban: String = raw.uppercase().replace(SEPARATOR, "")
            if (iban.length < 5) {
                throw CommonError.Payto("malformed IBAN, string is too small only ${iban.length} char")
            }
            for (c in iban.substring(0 until 2)) {
                if (!c.isLetter()) throw CommonError.Payto("malformed IBAN, malformed country code")
            }
            for (c in iban.substring(2 until 4)) {
                if (!c.isDigit()) throw CommonError.Payto("malformed IBAN, malformed check digit")
            }
            val checksum = checksum(iban)
            if (checksum != 1) throw CommonError.Payto("malformed IBAN, modulo is $checksum expected 1")
            return IBAN(iban)
        }

        fun rand(): IBAN {
            val bban = (0..10).map {
                (0..9).random()
            }.joinToString("") // 10 digits account number
            val checkDigits = 98 - checksum("DE00$bban");
            return if (checkDigits < 10) {
                IBAN("DE0$checkDigits$bban")
            } else {
                IBAN("DE$checkDigits$bban")
            }
        }
    }
}