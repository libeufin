/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024-2025 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */
package tech.libeufin.common

// DB
const val MIN_VERSION: Int = 14
const val SERIALIZATION_RETRY: Int = 30

// Security
const val MAX_BODY_LENGTH: Int = 4 * 1024 // 4kB

// API version
const val WIRE_GATEWAY_API_VERSION: String = "4:0:3"
const val REVENUE_API_VERSION: String = "1:1:1"

// HTTP headers
const val X_CHALLENGE_ID: String = "X-Challenge-Id"
const val X_FORWARD_PREFIX: String = "X-Forward-Prefix"

// Params
const val MAX_PAGE_SIZE: Int = 1024
const val MAX_TIMEOUT_MS: Long = 60 * 60 * 1000  // 1h
// TODO make MAX_TIMEOUT_MS configurable