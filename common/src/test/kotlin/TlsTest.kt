/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.plugins.*
import io.ktor.http.*
import org.junit.Test
import kotlin.io.path.Path
import kotlin.io.path.writeBytes
import kotlin.test.assertEquals
import kotlin.test.assertFails
import java.security.Security
import kotlinx.coroutines.runBlocking
import tech.libeufin.common.setupSecurityProperties

class TlsTest {
    @Test
    fun securityCheck() = runBlocking {
        setupSecurityProperties()
        
        val secureClient = HttpClient()
        val checks = sequenceOf(
            "expired",
            "wrong.host",
            "self-signed",
            "untrusted-root",
            "revoked",
            // "no-sct", TODO when java support this
            "preact-cli"
        )
        for (check in checks) {
            println("https://$check.badssl.com")
            assertFails {
                secureClient.get("https://$check.badssl.com")
            }
        }
    }
}