#!/bin/bash
set -evu

apt-get update
apt-get upgrade -yqq

./bootstrap
./configure --prefix /usr
make build

sudo -u postgres pg_ctlcluster 15 main start
sudo -u postgres createuser root --superuser
sudo -u postgres createdb -O root libeufincheck

check_command()
{
	make check &> test-suite.log
}

if ! check_command ; then
    cat test-suite.log
    exit 1
fi
