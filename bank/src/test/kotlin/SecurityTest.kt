/*
 * This file is part of LibEuFin.
 * Copyright (C) 2023-2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.http.content.*
import kotlinx.serialization.json.Json
import org.junit.Test
import tech.libeufin.common.*
import tech.libeufin.common.test.*

inline fun <reified B> HttpRequestBuilder.jsonDeflate(b: B) {
    val json = Json.encodeToString(kotlinx.serialization.serializer<B>(), b)
    contentType(ContentType.Application.Json)
    headers[HttpHeaders.ContentEncoding] = "deflate"
    setBody(json.toByteArray().inputStream().deflate().readBytes())
}

inline fun <reified B> HttpRequestBuilder.jsonStreamDeflate(b: B) {
    val json = Json.encodeToString(kotlinx.serialization.serializer<B>(), b)
    headers[HttpHeaders.ContentEncoding] = "deflate"
    setBody(OutputStreamContent({
        write(json.toByteArray().inputStream().deflate().readBytes())
    }, ContentType.Application.Json))
}

inline fun <reified B> HttpRequestBuilder.jsonStream(b: B) {
    val json = Json.encodeToString(kotlinx.serialization.serializer<B>(), b)
    setBody(OutputStreamContent({
        write(json.toByteArray())
    }, ContentType.Application.Json))
}

class SecurityTest {
    @Test
    fun bodySizeLimit() = bankSetup {
        val valid_req = obj {
            "payto_uri" to "$exchangePayto?message=payout"
            "amount" to "KUDOS:0.3"
        }
        val too_big = obj(valid_req) {
            "payto_uri" to "$exchangePayto?message=payout${"A".repeat(MAX_BODY_LENGTH+1)}"
        }

        client.postA("/accounts/merchant/transactions") {
            json(valid_req)
        }.assertOk()
        
        // Check body too big
        client.postA("/accounts/merchant/transactions") {
            json(too_big)
        }.assertBadRequest()

        // Check body too big even after compression
        client.postA("/accounts/merchant/transactions") {
            jsonDeflate(too_big)
        }.assertBadRequest()

        // Check streaming body too big
        client.postA("/accounts/merchant/transactions") {
            jsonStream(too_big)
        }.assertBadRequest()

        // Check streaming body too big even after compression
        client.postA("/accounts/merchant/transactions") {
            jsonStreamDeflate(too_big)
        }.assertBadRequest()

        // Check unknown encoding
        client.postA("/accounts/merchant/transactions") {
            headers[HttpHeaders.ContentEncoding] = "unknown"
            json(valid_req)
        }.assertStatus(HttpStatusCode.UnsupportedMediaType, TalerErrorCode.GENERIC_COMPRESSION_INVALID)
    }
}



