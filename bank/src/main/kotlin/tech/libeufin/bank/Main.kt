/*
 * This file is part of LibEuFin.
 * Copyright (C) 2023-2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.bank

import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tech.libeufin.bank.api.*
import tech.libeufin.bank.cli.LibeufinBank
import tech.libeufin.bank.db.Database
import tech.libeufin.common.api.talerApi
import com.github.ajalt.clikt.core.main

val logger: Logger = LoggerFactory.getLogger("libeufin-bank")
 
/** Set up web server handlers for the Taler corebank API */
fun Application.corebankWebApp(db: Database, ctx: BankConfig) = talerApi(logger) {
    coreBankApi(db, ctx)
    conversionApi(db, ctx)
    bankIntegrationApi(db, ctx)
    wireGatewayApi(db, ctx)
    revenueApi(db, ctx)
    ctx.spaPath?.let {
        get("/") {
            call.respondRedirect("/webui/")
        }
        staticFiles("/webui/", it.toFile())
    }
}

fun main(args: Array<String>) {
    LibeufinBank().main(args)
}
