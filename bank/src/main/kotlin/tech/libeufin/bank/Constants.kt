/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024-2025 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */
package tech.libeufin.bank

import tech.libeufin.common.ConfigSource
import java.time.Duration

// Config
val BANK_CONFIG_SOURCE = ConfigSource("libeufin", "libeufin-bank", "libeufin-bank")

// TAN
const val TAN_RETRY_COUNTER: Int = 3
val TAN_VALIDITY_PERIOD: Duration = Duration.ofMinutes(30)
val TAN_RETRANSMISSION_PERIOD: Duration = Duration.ofMinutes(3)

// Token
val TOKEN_DEFAULT_DURATION: Duration = Duration.ofHours(3)

// Account
val RESERVED_ACCOUNTS = setOf("admin", "bank") 
const val IBAN_ALLOCATION_RETRY_COUNTER: Int = 5
const val MAX_TOKEN_CREATION_ATTEMPTS: Int = 5
const val MAX_ACTIVE_CHALLENGES: Int = 5

// API version  
const val COREBANK_API_VERSION: String = "8:0:5"
const val CONVERSION_API_VERSION: String = "0:1:0"
const val INTEGRATION_API_VERSION: String = "5:0:5"
