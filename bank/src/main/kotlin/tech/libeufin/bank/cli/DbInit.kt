/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.bank.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.parameters.groups.provideDelegate
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import tech.libeufin.bank.bankConfig
import tech.libeufin.bank.createAdminAccount
import tech.libeufin.bank.db.AccountDAO.AccountCreationResult
import tech.libeufin.bank.logger
import tech.libeufin.bank.withDb
import tech.libeufin.common.CommonOption
import tech.libeufin.common.cliCmd
import tech.libeufin.common.db.dbInit
import tech.libeufin.common.db.pgDataSource

class DbInit : CliktCommand("dbinit") {
    override fun help(context: Context) = "Initialize the libeufin-bank database"

    private val common by CommonOption()
    private val reset by option(
        "--reset", "-r",
        help = "Reset database (DANGEROUS: All existing data is lost)"
    ).flag()

    override fun run() = cliCmd(logger, common.log) {
        val cfg = bankConfig(common.config)
        val dbCfg = cfg.dbCfg
        pgDataSource(dbCfg.dbConnStr).dbInit(dbCfg, "libeufin-bank", reset)
        cfg.withDb { db, cfg ->
            // Create admin account if missing
            val res = createAdminAccount(db, cfg)
            when (res) {
                AccountCreationResult.BonusBalanceInsufficient -> {}
                AccountCreationResult.UsernameReuse -> {}
                AccountCreationResult.PayToReuse -> 
                    throw Exception("Failed to create admin's account")
                is AccountCreationResult.Success ->
                    logger.info("Admin's account created")
            }
        }
    }
}