/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.bank.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.parameters.groups.provideDelegate
import tech.libeufin.bank.bankConfig
import tech.libeufin.bank.logger
import tech.libeufin.bank.withDb
import tech.libeufin.common.CommonOption
import tech.libeufin.common.cliCmd
import java.time.Instant

class GC : CliktCommand("gc") {
    override fun help(context: Context) = "Run garbage collection: abort expired operations and clean expired data"

    private val common by CommonOption()
 
    override fun run() = cliCmd(logger, common.log) {
        bankConfig(common.config).withDb { db, cfg ->
            logger.info("Run garbage collection")
            db.gc.collect(Instant.now(), cfg.gcAbortAfter, cfg.gcCleanAfter, cfg.gcDeleteAfter)
        }
    }
}