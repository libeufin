/*
 * This file is part of LibEuFin.
 * Copyright (C) 2023-2025 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.bank.db

import tech.libeufin.common.*
import tech.libeufin.common.db.*
import java.time.Instant

/** Data access logic for exchange specific logic */
class ExchangeDAO(private val db: Database) {
    /** Query [exchangeId] history of taler incoming transactions  */
    suspend fun incomingHistory(
        params: HistoryParams, 
        exchangeId: Long
    ): List<IncomingBankTransaction> 
        = db.poolHistory(params, exchangeId, db::listenIncoming,  """
            SELECT
                bank_transaction_id
                ,transaction_date
                ,(amount).val AS amount_val
                ,(amount).frac AS amount_frac
                ,debtor_payto
                ,debtor_name
                ,type
                ,metadata
            FROM taler_exchange_incoming AS tfr
                JOIN bank_account_transactions AS txs
                    ON bank_transaction=txs.bank_transaction_id
            WHERE
        """) {
            val type = it.getEnum<IncomingType>("type")
            when (type) {
                IncomingType.reserve -> IncomingReserveTransaction(
                    row_id = it.getLong("bank_transaction_id"),
                    date = it.getTalerTimestamp("transaction_date"),
                    amount = it.getAmount("amount", db.bankCurrency),
                    debit_account = it.getBankPayto("debtor_payto", "debtor_name", db.ctx),
                    reserve_pub = EddsaPublicKey(it.getBytes("metadata")),
                )
                IncomingType.kyc -> IncomingKycAuthTransaction(
                    row_id = it.getLong("bank_transaction_id"),
                    date = it.getTalerTimestamp("transaction_date"),
                    amount = it.getAmount("amount", db.bankCurrency),
                    debit_account = it.getBankPayto("debtor_payto", "debtor_name", db.ctx),
                    account_pub = EddsaPublicKey(it.getBytes("metadata")),
                )
                IncomingType.wad -> throw UnsupportedOperationException()
            }
        }
    
    /** Query [exchangeId] history of taler outgoing transactions  */
    suspend fun outgoingHistory(
        params: HistoryParams, 
        exchangeId: Long
    ): List<OutgoingTransaction> 
        = db.poolHistory(params, exchangeId, db::listenOutgoing,  """
            SELECT
                bank_transaction_id
                ,transaction_date
                ,(txs.amount).val AS amount_val
                ,(txs.amount).frac AS amount_frac
                ,txs.creditor_payto
                ,txs.creditor_name
                ,wtid
                ,exchange_base_url
            FROM taler_exchange_outgoing AS tfr
                JOIN transfer_operations USING (exchange_outgoing_id)
                JOIN bank_account_transactions AS txs
                    ON bank_transaction=txs.bank_transaction_id
            WHERE
        """) {
            OutgoingTransaction(
                row_id = it.getLong("bank_transaction_id"),
                date = it.getTalerTimestamp("transaction_date"),
                amount = it.getAmount("amount", db.bankCurrency),
                credit_account = it.getBankPayto("creditor_payto", "creditor_name", db.ctx),
                wtid = ShortHashCode(it.getBytes("wtid")),
                exchange_base_url = it.getString("exchange_base_url")
            )
        }

    /** Result of taler transfer transaction creation */
    sealed interface TransferResult {
        /** Transaction [id] and wire transfer [timestamp] */
        data class Success(val id: Long, val timestamp: TalerProtocolTimestamp): TransferResult
        data object NotAnExchange: TransferResult
        data object UnknownExchange: TransferResult
        data object BothPartyAreExchange: TransferResult
        data object BalanceInsufficient: TransferResult
        data object ReserveUidReuse: TransferResult
        data object WtidReuse: TransferResult
    }

    /** Perform a Taler transfer */
    suspend fun transfer(
        req: TransferRequest,
        username: String,
        timestamp: Instant
    ): TransferResult = db.serializable(
        """
        SELECT
            out_debtor_not_found
            ,out_debtor_not_exchange
            ,out_both_exchanges
            ,out_request_uid_reuse
            ,out_wtid_reuse
            ,out_exchange_balance_insufficient
            ,out_tx_row_id
            ,out_timestamp
        FROM
            taler_transfer (
                ?, ?, ?,
                (?,?)::taler_amount,
                ?, ?, ?, ?
            );
        """
    ) {
        val subject = "${req.wtid} ${req.exchange_base_url.url}"

        setBytes(1, req.request_uid.raw)
        setBytes(2, req.wtid.raw)
        setString(3, subject)
        setLong(4, req.amount.value)
        setInt(5, req.amount.frac)
        setString(6, req.exchange_base_url.url)
        setString(7, req.credit_account.canonical)
        setString(8, username)
        setLong(9, timestamp.micros())

        one {
            when {
                it.getBoolean("out_debtor_not_found") -> TransferResult.UnknownExchange
                it.getBoolean("out_debtor_not_exchange") -> TransferResult.NotAnExchange
                it.getBoolean("out_both_exchanges") -> TransferResult.BothPartyAreExchange
                it.getBoolean("out_exchange_balance_insufficient") -> TransferResult.BalanceInsufficient
                it.getBoolean("out_request_uid_reuse") -> TransferResult.ReserveUidReuse
                it.getBoolean("out_wtid_reuse") -> TransferResult.WtidReuse
                else -> TransferResult.Success(
                    id = it.getLong("out_tx_row_id"),
                    timestamp = it.getTalerTimestamp("out_timestamp")
                )
            }
        }
    }

    /** Get status of transfer [txId] of account [exchangeId] */
    suspend fun getTransfer(
        exchangeId: Long,
        txId: Long
    ): TransferStatus? = db.serializable(
        """
        SELECT
            wtid
            ,exchange_base_url
            ,transfer_date
            ,(amount).val AS amount_val
            ,(amount).frac AS amount_frac
            ,creditor_payto
            ,status
            ,status_msg
        FROM transfer_operations 
        WHERE transfer_operation_id=? AND exchange_id=?
        """
    ) {
        setLong(1, txId)
        setLong(2, exchangeId)
        oneOrNull {
            TransferStatus(
                status = it.getEnum<TransferStatusState>("status"),
                status_msg = it.getString("status_msg"),
                amount = it.getAmount("amount", db.bankCurrency),
                origin_exchange_url = it.getString("exchange_base_url"),
                wtid = ShortHashCode(it.getBytes("wtid")),
                credit_account = it.getBankPayto("creditor_payto", null, db.ctx),
                timestamp = it.getTalerTimestamp("transfer_date"),
            )
        }
    }

    /** Get a page of transfers status of account [exchangeId] */
    suspend fun pageTransfer(
        params: PageParams,
        exchangeId: Long,
        status: TransferStatusState?
    ): List<TransferListStatus> = db.page(
        params,
        "transfer_operation_id",
        """
        SELECT
            transfer_operation_id
            ,transfer_date
            ,(amount).val AS amount_val
            ,(amount).frac AS amount_frac
            ,creditor_payto
            ,status
        FROM transfer_operations
        WHERE exchange_id=? AND ${
            when (status) {
                null -> ""
                else -> "status=?::transfer_status AND"
            }               
        }
        """,
        {
            setLong(1, exchangeId)
            if (status == null) {
                1
            } else {
                setString(2, status.name)
                2
            }
        }
    ) {
        TransferListStatus(
            row_id = it.getLong("transfer_operation_id"),
            status = it.getEnum<TransferStatusState>("status"),
            amount = it.getAmount("amount", db.bankCurrency),
            credit_account = it.getBankPayto("creditor_payto", null, db.ctx),
            timestamp = it.getTalerTimestamp("transfer_date"),
        )
    }

    /** Result of taler add incoming transaction creation */
    sealed interface AddIncomingResult {
        /** Transaction [id] and wire transfer [timestamp] */
        data class Success(val id: Long, val timestamp: TalerProtocolTimestamp): AddIncomingResult
        data object NotAnExchange: AddIncomingResult
        data object UnknownExchange: AddIncomingResult
        data object UnknownDebtor: AddIncomingResult
        data object BothPartyAreExchange: AddIncomingResult
        data object ReservePubReuse: AddIncomingResult
        data object BalanceInsufficient: AddIncomingResult
    }

     /** Add a new taler incoming transaction */
    suspend fun addIncoming(
        amount: TalerAmount,
        debitAccount: Payto,
        subject: String,
        username: String,
        timestamp: Instant,
        metadata: IncomingSubject
    ): AddIncomingResult = db.serializable(
        """
        SELECT
            out_creditor_not_found
            ,out_creditor_not_exchange
            ,out_debtor_not_found
            ,out_both_exchanges
            ,out_reserve_pub_reuse
            ,out_debitor_balance_insufficient
            ,out_tx_row_id
        FROM
            taler_add_incoming (
                ?, ?, (?,?)::taler_amount,
                ?, ?, ?, ?::taler_incoming_type
            );
        """
    ) {
        setBytes(1, metadata.key.raw)
        setString(2, subject)
        setLong(3, amount.value)
        setInt(4, amount.frac)
        setString(5, debitAccount.canonical)
        setString(6, username)
        setLong(7, timestamp.micros())
        setString(8, metadata.type.name)

        one {
            when {
                it.getBoolean("out_creditor_not_found") -> AddIncomingResult.UnknownExchange 
                it.getBoolean("out_creditor_not_exchange") -> AddIncomingResult.NotAnExchange
                it.getBoolean("out_debtor_not_found") -> AddIncomingResult.UnknownDebtor
                it.getBoolean("out_both_exchanges") -> AddIncomingResult.BothPartyAreExchange
                it.getBoolean("out_debitor_balance_insufficient") -> AddIncomingResult.BalanceInsufficient
                it.getBoolean("out_reserve_pub_reuse") -> AddIncomingResult.ReservePubReuse
                else -> AddIncomingResult.Success(
                    id = it.getLong("out_tx_row_id"),
                    timestamp = TalerProtocolTimestamp(timestamp)
                )
            }
        }
    }
}