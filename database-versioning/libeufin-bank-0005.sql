--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

BEGIN;

SELECT _v.register_patch('libeufin-bank-0005', NULL, NULL);
SET search_path TO libeufin_bank;

-- Make withdrawal amount optional and add optional suggested_amount
ALTER TABLE taler_withdrawal_operations ADD suggested_amount taler_amount;
ALTER TABLE taler_withdrawal_operations ALTER COLUMN amount DROP NOT NULL;

-- Add description and last_access to bearer_tokens
ALTER TABLE bearer_tokens ADD description TEXT;
ALTER TABLE bearer_tokens ADD last_access INT8;
UPDATE bearer_tokens SET last_access=creation_time;
ALTER TABLE bearer_tokens ALTER COLUMN last_access SET NOT NULL;

-- Add new token scope 'revenue'
ALTER TYPE token_scope_enum ADD VALUE 'revenue';

COMMIT;
