--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

BEGIN;

SELECT _v.register_patch('libeufin-nexus-0007', NULL, NULL);

SET search_path TO libeufin_nexus;

-- Add a new submission state reusing a currently unused slot
ALTER TYPE submission_state RENAME VALUE 'never_heard_back' TO 'pending';
ALTER TYPE submission_state ADD VALUE 'late_failure';

-- Batch of initiated_outgoing_transactions
CREATE TABLE initiated_outgoing_batches(
    initiated_outgoing_batch_id INT8 GENERATED BY DEFAULT AS IDENTITY UNIQUE,
    creation_date INT8 NOT NULL,
    sum taler_amount NOT NULL DEFAULT (0, 0),
    message_id TEXT NOT NULL UNIQUE CHECK (char_length(message_id) <= 35),
    order_id TEXT UNIQUE,
    submission_date INT8,
    submission_counter INT4 NOT NULL DEFAULT 0,
    status submission_state NOT NULL DEFAULT 'unsubmitted',
    status_msg TEXT
);
COMMENT ON COLUMN initiated_outgoing_transactions.order_id
  IS 'Order ID of the EBICS upload transaction, used to track EBICS order status.';

-- Add batch column to initiated_outgoing_transactions
ALTER TABLE initiated_outgoing_transactions 
    ADD COLUMN initiated_outgoing_batch_id INT8 REFERENCES initiated_outgoing_batches (initiated_outgoing_batch_id);

-- Create a batch for all existing initiated_outgoing_transactions
INSERT INTO initiated_outgoing_batches(creation_date, message_id, order_id, submission_date, submission_counter, status, status_msg)
    SELECT initiation_time, request_uid, order_id, last_submission_time, submission_counter, (CASE WHEN submitted = 'success' OR submitted = 'permanent_failure' THEN 'success' ELSE 'pending' END)::submission_state, failure_message
    FROM initiated_outgoing_transactions;

-- Link initiated_outgoing_transactions to their initiated_outgoing_batches
UPDATE initiated_outgoing_transactions SET initiated_outgoing_batch_id = (
    SELECT initiated_outgoing_batch_id FROM initiated_outgoing_batches WHERE request_uid=message_id
);

-- Drop now unused columns from initiated_outgoing_transactions and rename some
ALTER TABLE initiated_outgoing_transactions 
    DROP COLUMN order_id,
    DROP COLUMN last_submission_time,
    DROP COLUMN submission_counter,
    DROP COLUMN hidden;

-- Add necessary indexes
CREATE INDEX initiated_outgoing_batches_status_index ON initiated_outgoing_batches (status);
COMMENT ON INDEX initiated_outgoing_batches_status_index IS 'for listing taler batch by status for a future admin UI';
CREATE INDEX initiated_outgoing_transactions_batch_index ON initiated_outgoing_transactions (initiated_outgoing_batch_id);
COMMENT ON INDEX initiated_outgoing_transactions_batch_index IS 'for listing transactions in batches';

-- Renaming
ALTER TABLE incoming_transactions RENAME COLUMN wire_transfer_subject TO subject;
ALTER TABLE incoming_transactions RENAME COLUMN debit_payto_uri TO debit_payto;
ALTER TABLE outgoing_transactions RENAME COLUMN wire_transfer_subject TO subject;
ALTER TABLE outgoing_transactions RENAME COLUMN credit_payto_uri TO credit_payto;
ALTER TABLE outgoing_transactions RENAME COLUMN message_id TO end_to_end_id;
ALTER TABLE initiated_outgoing_transactions RENAME COLUMN wire_transfer_subject TO subject;
ALTER TABLE initiated_outgoing_transactions RENAME COLUMN credit_payto_uri TO credit_payto;
ALTER TABLE initiated_outgoing_transactions RENAME COLUMN submitted TO status;
ALTER TABLE initiated_outgoing_transactions RENAME COLUMN failure_message TO status_msg;
ALTER TABLE initiated_outgoing_transactions RENAME COLUMN request_uid TO end_to_end_id;
COMMIT;
