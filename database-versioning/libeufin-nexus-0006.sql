--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

BEGIN;

SELECT _v.register_patch('libeufin-nexus-0006', NULL, NULL);

SET search_path TO libeufin_nexus;

-- Support all taler incoming transaction types
CREATE TYPE taler_incoming_type AS ENUM
  ('reserve' ,'kyc', 'wad');
ALTER TABLE talerable_incoming_transactions
  ADD type taler_incoming_type NOT NULL DEFAULT 'reserve',
  ADD account_pub BYTEA CHECK (LENGTH(account_pub)=32),
  ADD origin_exchange_url TEXT,
  ADD wad_id BYTEA CHECK (LENGTH(wad_id)=24),
  ALTER COLUMN reserve_public_key DROP NOT NULL,
  ADD CONSTRAINT incoming_polymorphism CHECK(
    CASE type
      WHEN 'reserve' THEN reserve_public_key IS NOT NULL AND account_pub IS NULL AND origin_exchange_url IS NULL AND wad_id IS NULL
      WHEN 'kyc' THEN reserve_public_key IS NULL AND account_pub IS NOT NULL AND origin_exchange_url IS NULL AND wad_id IS NULL
      WHEN 'wad' THEN reserve_public_key IS NULL AND account_pub IS NULL AND origin_exchange_url IS NOT NULL AND wad_id IS NOT NULL
    END
  );
ALTER TABLE talerable_incoming_transactions ALTER COLUMN type DROP DEFAULT;

CREATE INDEX talerable_incoming_transactions_kyc_index ON talerable_incoming_transactions (account_pub) WHERE account_pub IS NOT NULL;
COMMENT ON INDEX talerable_incoming_transactions_kyc_index IS 'for reconciling KYC transaction without bank_id';

CREATE INDEX initiated_outgoing_transactions_status_index ON initiated_outgoing_transactions (submitted);
COMMENT ON INDEX initiated_outgoing_transactions_status_index IS 'for listing taler transfers by status';

COMMIT;
