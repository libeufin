--
-- This file is part of TALER
-- Copyright (C) 2023-2025 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

BEGIN;
SET search_path TO libeufin_bank;

-- Remove all existing functions
DO
$do$
DECLARE
  _sql text;
BEGIN
  SELECT INTO _sql
        string_agg(format('DROP %s %s CASCADE;'
                        , CASE prokind
                            WHEN 'f' THEN 'FUNCTION'
                            WHEN 'p' THEN 'PROCEDURE'
                          END
                        , oid::regprocedure)
                  , E'\n')
  FROM   pg_proc
  WHERE  pronamespace = 'libeufin_bank'::regnamespace;

  IF _sql IS NOT NULL THEN
    EXECUTE _sql;
  END IF;
END
$do$;

CREATE FUNCTION amount_normalize(
    IN amount taler_amount
  ,OUT normalized taler_amount
)
LANGUAGE plpgsql IMMUTABLE AS $$
BEGIN
  normalized.val = amount.val + amount.frac / 100000000;
  IF (normalized.val > 1::INT8<<52) THEN
    RAISE EXCEPTION 'amount value overflowed';
  END IF;
  normalized.frac = amount.frac % 100000000;

END $$;
COMMENT ON FUNCTION amount_normalize
  IS 'Returns the normalized amount by adding to the .val the value of (.frac / 100000000) and removing the modulus 100000000 from .frac.'
      'It raises an exception when the resulting .val is larger than 2^52';

CREATE FUNCTION amount_add(
   IN l taler_amount
  ,IN r taler_amount
  ,OUT sum taler_amount
)
LANGUAGE plpgsql IMMUTABLE AS $$
BEGIN
  sum = (l.val + r.val, l.frac + r.frac);
  SELECT normalized.val, normalized.frac INTO sum.val, sum.frac FROM amount_normalize(sum) as normalized;
END $$;
COMMENT ON FUNCTION amount_add
  IS 'Returns the normalized sum of two amounts. It raises an exception when the resulting .val is larger than 2^52';

CREATE FUNCTION amount_left_minus_right(
  IN l taler_amount
 ,IN r taler_amount
 ,OUT diff taler_amount
 ,OUT ok BOOLEAN
)
LANGUAGE plpgsql IMMUTABLE AS $$
BEGIN
diff = l;
IF diff.frac < r.frac THEN
  IF diff.val <= 0 THEN
    diff = (-1, -1);
    ok = FALSE;
    RETURN;
  END IF;
  diff.frac = diff.frac + 100000000;
  diff.val = diff.val - 1;
END IF;
IF diff.val < r.val THEN
  diff = (-1, -1);
  ok = FALSE;
  RETURN;
END IF;
diff.val = diff.val - r.val;
diff.frac = diff.frac - r.frac;
ok = TRUE;
END $$;
COMMENT ON FUNCTION amount_left_minus_right
  IS 'Subtracts the right amount from the left and returns the difference and TRUE, if the left amount is larger than the right, or an invalid amount and FALSE otherwise.';

CREATE FUNCTION account_balance_is_sufficient(
  IN in_account_id INT8,
  IN in_amount taler_amount,
  IN in_wire_transfer_fees taler_amount,
  IN in_min_amount taler_amount,
  IN in_max_amount taler_amount,
  OUT out_balance_insufficient BOOLEAN,
  OUT out_bad_amount BOOLEAN
)
LANGUAGE plpgsql AS $$ 
DECLARE
account_has_debt BOOLEAN;
account_balance taler_amount;
account_max_debt taler_amount;
amount_with_fee taler_amount;
BEGIN

-- Check min and max
SELECT (SELECT in_min_amount IS NOT NULL AND NOT ok FROM amount_left_minus_right(in_amount, in_min_amount)) OR
  (SELECT in_max_amount IS NOT NULL AND NOT ok FROM amount_left_minus_right(in_max_amount, in_amount))
  INTO out_bad_amount;
IF out_bad_amount THEN
  RETURN;
END IF;

-- Add fees to the amount
IF in_wire_transfer_fees IS NOT NULL AND in_wire_transfer_fees != (0, 0)::taler_amount THEN
  SELECT sum.val, sum.frac 
    INTO amount_with_fee.val, amount_with_fee.frac 
    FROM amount_add(in_amount, in_wire_transfer_fees) as sum;
ELSE
  amount_with_fee = in_amount;
END IF;

-- Get account info, we expect the account to exist
SELECT
  has_debt,
  (balance).val, (balance).frac,
  (max_debt).val, (max_debt).frac
  INTO
    account_has_debt,
    account_balance.val, account_balance.frac,
    account_max_debt.val, account_max_debt.frac
  FROM bank_accounts WHERE bank_account_id=in_account_id;

-- Check enough funds
IF account_has_debt THEN 
  -- debt case: simply checking against the max debt allowed.
  SELECT sum.val, sum.frac 
    INTO account_balance.val, account_balance.frac 
    FROM amount_add(account_balance, amount_with_fee) as sum;
  SELECT NOT ok
    INTO out_balance_insufficient
    FROM amount_left_minus_right(account_max_debt, account_balance);
  IF out_balance_insufficient THEN
    RETURN;
  END IF;
ELSE -- not a debt account
  SELECT NOT ok
    INTO out_balance_insufficient
    FROM amount_left_minus_right(account_balance, amount_with_fee);
  IF out_balance_insufficient THEN
     -- debtor will switch to debt: determine their new negative balance.
    SELECT
      (diff).val, (diff).frac
      INTO
        account_balance.val, account_balance.frac
      FROM amount_left_minus_right(amount_with_fee, account_balance);
    SELECT NOT ok
      INTO out_balance_insufficient
      FROM amount_left_minus_right(account_max_debt, account_balance);
    IF out_balance_insufficient THEN
      RETURN;
    END IF;
  END IF;
END IF;
END $$;
COMMENT ON FUNCTION account_balance_is_sufficient IS 'Check if an account have enough fund to transfer an amount.';

CREATE FUNCTION account_max_amount(
  IN in_account_id INT8,
  IN in_max_amount taler_amount,
  OUT out_max_amount taler_amount
)
LANGUAGE plpgsql AS $$
BEGIN
-- add balance and max_debt
WITH computed AS (
  SELECT CASE has_debt
    WHEN false THEN amount_add(balance, max_debt)
    ELSE (SELECT diff FROM amount_left_minus_right(max_debt, balance))
  END AS amount 
  FROM bank_accounts WHERE bank_account_id=in_account_id
) SELECT (amount).val, (amount).frac
  INTO out_max_amount.val, out_max_amount.frac
  FROM computed;

IF in_max_amount.val < out_max_amount.val 
  OR (in_max_amount.val = out_max_amount.val OR in_max_amount.frac < out_max_amount.frac) THEN
  out_max_amount = in_max_amount;
END IF;
END $$;

CREATE FUNCTION create_token(
  IN in_username TEXT,
  IN in_content BYTEA,
  IN in_creation_time INT8,
  IN in_expiration_time INT8,
  IN in_scope token_scope_enum,
  IN in_refreshable BOOLEAN,
  IN in_description TEXT,
  IN in_is_tan BOOLEAN,
  OUT out_tan_required BOOLEAN
)
LANGUAGE plpgsql AS $$
DECLARE
local_customer_id INT8;
BEGIN
-- Get account id and check if 2FA is required
SELECT customer_id, (NOT in_is_tan AND tan_channel IS NOT NULL)
INTO local_customer_id, out_tan_required
FROM customers JOIN bank_accounts ON owning_customer_id = customer_id
WHERE username = in_username AND deleted_at IS NULL;
IF out_tan_required THEN
  RETURN;
END IF;
INSERT INTO bearer_tokens (
  content,
  creation_time,
  expiration_time,
  scope,
  bank_customer,
  is_refreshable,
  description,
  last_access
) VALUES (
  in_content,
  in_creation_time,
  in_expiration_time,
  in_scope,
  local_customer_id,
  in_refreshable,
  in_description,
  in_creation_time
);
END $$;

CREATE FUNCTION bank_wire_transfer(
  IN in_creditor_account_id INT8,
  IN in_debtor_account_id INT8,
  IN in_subject TEXT,
  IN in_amount taler_amount,
  IN in_timestamp INT8,
  IN in_wire_transfer_fees taler_amount,
  IN in_min_amount taler_amount,
  IN in_max_amount taler_amount,
  -- Error status
  OUT out_balance_insufficient BOOLEAN,
  OUT out_bad_amount BOOLEAN,
  -- Success return
  OUT out_credit_row_id INT8,
  OUT out_debit_row_id INT8
)
LANGUAGE plpgsql AS $$
DECLARE
has_fee BOOLEAN;
amount_with_fee taler_amount;
admin_account_id INT8;
admin_has_debt BOOLEAN;
admin_balance taler_amount;
admin_payto TEXT;
admin_name TEXT;
debtor_has_debt BOOLEAN;
debtor_balance taler_amount;
debtor_max_debt taler_amount;
debtor_payto TEXT;
debtor_name TEXT;
creditor_has_debt BOOLEAN;
creditor_balance taler_amount;
creditor_payto TEXT;
creditor_name TEXT;
tmp_balance taler_amount;
BEGIN
-- Check min and max
SELECT (SELECT in_min_amount IS NOT NULL AND NOT ok FROM amount_left_minus_right(in_amount, in_min_amount)) OR
  (SELECT in_max_amount IS NOT NULL AND NOT ok FROM amount_left_minus_right(in_max_amount, in_amount))
  INTO out_bad_amount;
IF out_bad_amount THEN
  RETURN;
END IF;

has_fee = in_wire_transfer_fees IS NOT NULL AND in_wire_transfer_fees != (0, 0)::taler_amount;
IF has_fee THEN
  -- Retrieve admin info
  SELECT
    bank_account_id, has_debt,
    (balance).val, (balance).frac,
    internal_payto, customers.name
    INTO 
      admin_account_id, admin_has_debt,
      admin_balance.val, admin_balance.frac,
      admin_payto, admin_name
    FROM bank_accounts
      JOIN customers ON customer_id=owning_customer_id
    WHERE username = 'admin';
  IF NOT FOUND THEN
    RAISE EXCEPTION 'No admin';
  END IF;
END IF;

-- Retrieve debtor info
SELECT
  has_debt,
  (balance).val, (balance).frac,
  (max_debt).val, (max_debt).frac,
  internal_payto, customers.name
  INTO
    debtor_has_debt,
    debtor_balance.val, debtor_balance.frac,
    debtor_max_debt.val, debtor_max_debt.frac,
    debtor_payto, debtor_name
  FROM bank_accounts
    JOIN customers ON customer_id=owning_customer_id
  WHERE bank_account_id=in_debtor_account_id;
IF NOT FOUND THEN
  RAISE EXCEPTION 'Unknown debtor %', in_debtor_account_id;
END IF;
-- Retrieve creditor info
SELECT
  has_debt,
  (balance).val, (balance).frac,
  internal_payto, customers.name
  INTO
    creditor_has_debt,
    creditor_balance.val, creditor_balance.frac,
    creditor_payto, creditor_name
  FROM bank_accounts
    JOIN customers ON customer_id=owning_customer_id
  WHERE bank_account_id=in_creditor_account_id;
IF NOT FOUND THEN
  RAISE EXCEPTION 'Unknown creditor %', in_creditor_account_id;
END IF;

-- Add fees to the amount
IF has_fee AND admin_account_id != in_debtor_account_id THEN
  SELECT sum.val, sum.frac 
    INTO amount_with_fee.val, amount_with_fee.frac 
    FROM amount_add(in_amount, in_wire_transfer_fees) as sum;
ELSE
  has_fee=false;
  amount_with_fee = in_amount;
END IF;

-- DEBTOR SIDE
-- check debtor has enough funds.
IF debtor_has_debt THEN 
  -- debt case: simply checking against the max debt allowed.
  SELECT sum.val, sum.frac 
    INTO debtor_balance.val, debtor_balance.frac 
    FROM amount_add(debtor_balance, amount_with_fee) as sum;
  SELECT NOT ok
    INTO out_balance_insufficient
    FROM amount_left_minus_right(debtor_max_debt,
                                 debtor_balance);
  IF out_balance_insufficient THEN
    RETURN;
  END IF;
ELSE -- not a debt account
  SELECT
    NOT ok,
    (diff).val, (diff).frac
    INTO
      out_balance_insufficient,
      tmp_balance.val,
      tmp_balance.frac
    FROM amount_left_minus_right(debtor_balance,
                                 amount_with_fee);
  IF NOT out_balance_insufficient THEN -- debtor has enough funds in the (positive) balance.
    debtor_balance=tmp_balance;
  ELSE -- debtor will switch to debt: determine their new negative balance.
    SELECT
      (diff).val, (diff).frac
      INTO
        debtor_balance.val, debtor_balance.frac
      FROM amount_left_minus_right(amount_with_fee,
                                   debtor_balance);
    debtor_has_debt=TRUE;
    SELECT NOT ok
      INTO out_balance_insufficient
      FROM amount_left_minus_right(debtor_max_debt,
                                   debtor_balance);
    IF out_balance_insufficient THEN
      RETURN;
    END IF;
  END IF;
END IF;

-- CREDITOR SIDE.
-- Here we figure out whether the creditor would switch
-- from debit to a credit situation, and adjust the balance
-- accordingly.
IF NOT creditor_has_debt THEN -- easy case.
  SELECT sum.val, sum.frac 
    INTO creditor_balance.val, creditor_balance.frac 
    FROM amount_add(creditor_balance, in_amount) as sum;
ELSE -- creditor had debit but MIGHT switch to credit.
  SELECT
    (diff).val, (diff).frac,
    NOT ok
    INTO
      tmp_balance.val, tmp_balance.frac,
      creditor_has_debt
    FROM amount_left_minus_right(in_amount,
                                 creditor_balance);
  IF NOT creditor_has_debt THEN
    creditor_balance=tmp_balance;
  ELSE
    -- the amount is not enough to bring the receiver
    -- to a credit state, switch operators to calculate the new balance.
    SELECT
      (diff).val, (diff).frac
      INTO creditor_balance.val, creditor_balance.frac
      FROM amount_left_minus_right(creditor_balance,
	                           in_amount);
  END IF;
END IF;

-- ADMIN SIDE.
-- Here we figure out whether the administrator would switch
-- from debit to a credit situation, and adjust the balance
-- accordingly.
IF has_fee THEN
  IF NOT admin_has_debt THEN -- easy case.
    SELECT sum.val, sum.frac 
      INTO admin_balance.val, admin_balance.frac 
      FROM amount_add(admin_balance, in_wire_transfer_fees) as sum;
  ELSE -- creditor had debit but MIGHT switch to credit.
    SELECT (diff).val, (diff).frac, NOT ok
      INTO
        tmp_balance.val, tmp_balance.frac,
        admin_has_debt
      FROM amount_left_minus_right(in_wire_transfer_fees, admin_balance);
    IF NOT admin_has_debt THEN
      admin_balance=tmp_balance;
    ELSE
      -- the amount is not enough to bring the receiver
      -- to a credit state, switch operators to calculate the new balance.
      SELECT (diff).val, (diff).frac
        INTO admin_balance.val, admin_balance.frac
        FROM amount_left_minus_right(admin_balance, in_wire_transfer_fees);
    END IF;
  END IF;
END IF;

-- Lock account in order to prevent deadlocks
PERFORM FROM bank_accounts
  WHERE bank_account_id IN (in_debtor_account_id, in_creditor_account_id, admin_account_id)
  ORDER BY bank_account_id
  FOR UPDATE;

-- now actually create the bank transaction.
-- debtor side:
INSERT INTO bank_account_transactions (
  creditor_payto
  ,creditor_name
  ,debtor_payto
  ,debtor_name
  ,subject
  ,amount
  ,transaction_date
  ,direction
  ,bank_account_id
  )
VALUES (
  creditor_payto,
  creditor_name,
  debtor_payto,
  debtor_name,
  in_subject,
  in_amount,
  in_timestamp,
  'debit',
  in_debtor_account_id
) RETURNING bank_transaction_id INTO out_debit_row_id;

-- debtor side:
INSERT INTO bank_account_transactions (
  creditor_payto
  ,creditor_name
  ,debtor_payto
  ,debtor_name
  ,subject
  ,amount
  ,transaction_date
  ,direction
  ,bank_account_id
  )
VALUES (
  creditor_payto,
  creditor_name,
  debtor_payto,
  debtor_name,
  in_subject,
  in_amount,
  in_timestamp,
  'credit',
  in_creditor_account_id
) RETURNING bank_transaction_id INTO out_credit_row_id;

-- checks and balances set up, now update bank accounts.
UPDATE bank_accounts
SET
  balance=debtor_balance,
  has_debt=debtor_has_debt
WHERE bank_account_id=in_debtor_account_id;

UPDATE bank_accounts
SET
  balance=creditor_balance,
  has_debt=creditor_has_debt
WHERE bank_account_id=in_creditor_account_id;

-- Fee part
IF has_fee THEN
  INSERT INTO bank_account_transactions (
    creditor_payto
    ,creditor_name
    ,debtor_payto
    ,debtor_name
    ,subject
    ,amount
    ,transaction_date
    ,direction
    ,bank_account_id
    )
  VALUES (
    admin_payto,
    admin_name,
    debtor_payto,
    debtor_name,
    'wire transfer fees for tx ' || out_debit_row_id,
    in_wire_transfer_fees,
    in_timestamp,
    'debit',
    in_debtor_account_id
  ), (
    admin_payto,
    admin_name,
    debtor_payto,
    debtor_name,
    'wire transfer fees for tx ' || out_debit_row_id,
    in_amount,
    in_timestamp,
    'credit',
    admin_account_id
  );

  UPDATE bank_accounts
  SET
    balance=admin_balance,
    has_debt=admin_has_debt
  WHERE bank_account_id=admin_account_id;
END IF;

-- notify new transaction
PERFORM pg_notify('bank_tx', in_debtor_account_id || ' ' || in_creditor_account_id || ' ' || out_debit_row_id || ' ' || out_credit_row_id);
END $$;

CREATE FUNCTION account_delete(
  IN in_username TEXT,
  IN in_timestamp INT8,
  IN in_is_tan BOOLEAN,
  OUT out_not_found BOOLEAN,
  OUT out_balance_not_zero BOOLEAN,
  OUT out_tan_required BOOLEAN
)
LANGUAGE plpgsql AS $$
DECLARE
my_customer_id INT8;
BEGIN
-- check if account exists, has zero balance and if 2FA is required
SELECT 
   customer_id
  ,(NOT in_is_tan AND tan_channel IS NOT NULL)
  ,((balance).val != 0 OR (balance).frac != 0)
  INTO 
     my_customer_id
    ,out_tan_required
    ,out_balance_not_zero
  FROM customers 
    JOIN bank_accounts ON owning_customer_id = customer_id
  WHERE username = in_username AND deleted_at IS NULL;
IF NOT FOUND OR out_balance_not_zero OR out_tan_required THEN
  out_not_found=NOT FOUND;
  RETURN;
END IF;

-- actual deletion
UPDATE customers SET deleted_at = in_timestamp WHERE customer_id = my_customer_id;
END $$;
COMMENT ON FUNCTION account_delete IS 'Deletes an account if the balance is zero';

CREATE PROCEDURE register_incoming(
  IN in_tx_row_id INT8,
  IN in_type taler_incoming_type,
  IN in_metadata BYTEA,
  IN in_account_id INT8
)
LANGUAGE plpgsql AS $$
DECLARE
local_amount taler_amount;
BEGIN
-- Register incoming transaction
INSERT INTO taler_exchange_incoming (
  metadata,
  bank_transaction,
  type
) VALUES (
  in_metadata,
  in_tx_row_id,
  in_type
);
-- Update stats
IF in_type = 'reserve' THEN
  SELECT (amount).val, (amount).frac
    INTO local_amount.val, local_amount.frac
    FROM bank_account_transactions WHERE bank_transaction_id=in_tx_row_id;
  CALL stats_register_payment('taler_in', NULL, local_amount, null);
END IF;
-- Notify new incoming transaction
PERFORM pg_notify('bank_incoming_tx', in_account_id || ' ' || in_tx_row_id);
END $$;
COMMENT ON PROCEDURE register_incoming
  IS 'Register a bank transaction as a taler incoming transaction and announce it';


CREATE FUNCTION taler_transfer(
  IN in_request_uid BYTEA,
  IN in_wtid BYTEA,
  IN in_subject TEXT,
  IN in_amount taler_amount,
  IN in_exchange_base_url TEXT,
  IN in_credit_account_payto TEXT,
  IN in_username TEXT,
  IN in_timestamp INT8,
  -- Error status
  OUT out_debtor_not_found BOOLEAN,
  OUT out_debtor_not_exchange BOOLEAN,
  OUT out_both_exchanges BOOLEAN,
  OUT out_request_uid_reuse BOOLEAN,
  OUT out_wtid_reuse BOOLEAN,
  OUT out_exchange_balance_insufficient BOOLEAN,
  -- Success return
  OUT out_tx_row_id INT8,
  OUT out_timestamp INT8
)
LANGUAGE plpgsql AS $$
DECLARE
exchange_account_id INT8;
creditor_account_id INT8;
creditor_name TEXT;
credit_row_id INT8;
debit_row_id INT8;
outgoing_id INT8;
BEGIN
-- Check for idempotence and conflict
SELECT (amount != in_amount 
          OR creditor_payto != in_credit_account_payto
          OR exchange_base_url != in_exchange_base_url
          OR wtid != in_wtid)
        ,transfer_operation_id, transfer_date
  INTO out_request_uid_reuse, out_tx_row_id, out_timestamp
  FROM transfer_operations
  WHERE request_uid = in_request_uid;
IF found THEN
  RETURN;
END IF;
out_wtid_reuse = EXISTS(SELECT FROM transfer_operations WHERE wtid = in_wtid);
IF out_wtid_reuse THEN
  RETURN;
END IF;
out_timestamp=in_timestamp;
-- Find exchange bank account id
SELECT
  bank_account_id, NOT is_taler_exchange
  INTO exchange_account_id, out_debtor_not_exchange
  FROM bank_accounts 
      JOIN customers 
        ON customer_id=owning_customer_id
  WHERE username = in_username AND deleted_at IS NULL;
out_debtor_not_found=NOT FOUND;
IF out_debtor_not_found OR out_debtor_not_exchange THEN
  RETURN;
END IF;
-- Find creditor bank account id
SELECT
  bank_account_id, is_taler_exchange
  INTO creditor_account_id, out_both_exchanges
  FROM bank_accounts
  WHERE internal_payto = in_credit_account_payto;
IF NOT FOUND THEN
  -- Register failure
  INSERT INTO transfer_operations (
    request_uid,
    wtid,
    amount,
    exchange_base_url,
    transfer_date,
    exchange_outgoing_id,
    creditor_payto,
    status,
    status_msg,
    exchange_id
  ) VALUES (
    in_request_uid,
    in_wtid,
    in_amount,
    in_exchange_base_url,
    in_timestamp,
    NULL,
    in_credit_account_payto,
    'permanent_failure',
    'Unknown account',
    exchange_account_id
  ) RETURNING transfer_operation_id INTO out_tx_row_id;
  RETURN;
ELSIF out_both_exchanges THEN
  RETURN;
END IF;
-- Perform bank transfer
SELECT
  out_balance_insufficient,
  out_debit_row_id, out_credit_row_id
  INTO
    out_exchange_balance_insufficient,
    debit_row_id, credit_row_id
  FROM bank_wire_transfer(
    creditor_account_id,
    exchange_account_id,
    in_subject,
    in_amount,
    in_timestamp,
    NULL,
    NULL,
    NULL
  );
IF out_exchange_balance_insufficient THEN
  RETURN;
END IF;
-- Register outgoing transaction
INSERT INTO taler_exchange_outgoing (
  bank_transaction
) VALUES (
  debit_row_id
) RETURNING exchange_outgoing_id INTO outgoing_id;
-- Update stats
CALL stats_register_payment('taler_out', NULL, in_amount, null);
-- Register success
INSERT INTO transfer_operations (
  request_uid,
  wtid,
  amount,
  exchange_base_url,
  transfer_date,
  exchange_outgoing_id,
  creditor_payto,
  status,
  status_msg,
  exchange_id
) VALUES (
  in_request_uid,
  in_wtid,
  in_amount,
  in_exchange_base_url,
  in_timestamp,
  outgoing_id,
  in_credit_account_payto,
  'success',
  NULL,
  exchange_account_id
) RETURNING transfer_operation_id INTO out_tx_row_id;

-- Notify new transaction
PERFORM pg_notify('bank_outgoing_tx', exchange_account_id || ' ' || creditor_account_id || ' ' || debit_row_id || ' ' || credit_row_id);
END $$;
COMMENT ON FUNCTION taler_transfer IS 'Create an outgoing taler transaction and register it';

CREATE FUNCTION taler_add_incoming(
  IN in_key BYTEA,
  IN in_subject TEXT,
  IN in_amount taler_amount,
  IN in_debit_account_payto TEXT,
  IN in_username TEXT,
  IN in_timestamp INT8,
  IN in_type taler_incoming_type,
  -- Error status
  OUT out_creditor_not_found BOOLEAN,
  OUT out_creditor_not_exchange BOOLEAN,
  OUT out_debtor_not_found BOOLEAN,
  OUT out_both_exchanges BOOLEAN,
  OUT out_reserve_pub_reuse BOOLEAN,
  OUT out_debitor_balance_insufficient BOOLEAN,
  -- Success return
  OUT out_tx_row_id INT8
)
LANGUAGE plpgsql AS $$
DECLARE
exchange_bank_account_id INT8;
sender_bank_account_id INT8;
BEGIN
-- Check conflict
IF in_type = 'reserve'::taler_incoming_type THEN
  SELECT EXISTS(SELECT FROM taler_exchange_incoming WHERE metadata = in_key AND type = 'reserve') OR
    EXISTS(SELECT FROM taler_withdrawal_operations WHERE reserve_pub = in_key) 
    INTO out_reserve_pub_reuse;
  IF out_reserve_pub_reuse THEN
    RETURN;
  END IF;
END IF;
-- Find exchange bank account id
SELECT
  bank_account_id, NOT is_taler_exchange
  INTO exchange_bank_account_id, out_creditor_not_exchange
  FROM bank_accounts 
      JOIN customers 
        ON customer_id=owning_customer_id
  WHERE username = in_username AND deleted_at IS NULL;
IF NOT FOUND OR out_creditor_not_exchange THEN
  out_creditor_not_found=NOT FOUND;
  RETURN;
END IF;
-- Find sender bank account id
SELECT
  bank_account_id, is_taler_exchange
  INTO sender_bank_account_id, out_both_exchanges
  FROM bank_accounts
  WHERE internal_payto = in_debit_account_payto;
IF NOT FOUND OR out_both_exchanges THEN
  out_debtor_not_found=NOT FOUND;
  RETURN;
END IF;
-- Perform bank transfer
SELECT
  out_balance_insufficient,
  out_credit_row_id
  INTO
    out_debitor_balance_insufficient,
    out_tx_row_id
  FROM bank_wire_transfer(
    exchange_bank_account_id,
    sender_bank_account_id,
    in_subject,
    in_amount,
    in_timestamp,
    NULL,
    NULL,
    NULL
  ) as transfer;
IF out_debitor_balance_insufficient THEN
  RETURN;
END IF;
-- Register incoming transaction
CALL register_incoming(out_tx_row_id, in_type, in_key, exchange_bank_account_id);
END $$;
COMMENT ON FUNCTION taler_add_incoming IS 'Create an incoming taler transaction and register it';

CREATE FUNCTION bank_transaction(
  IN in_credit_account_payto TEXT,
  IN in_debit_account_username TEXT,
  IN in_subject TEXT,
  IN in_amount taler_amount,
  IN in_timestamp INT8,
  IN in_is_tan BOOLEAN,
  IN in_request_uid BYTEA,
  IN in_wire_transfer_fees taler_amount,
  IN in_min_amount taler_amount,
  IN in_max_amount taler_amount,
  -- Error status
  OUT out_creditor_not_found BOOLEAN,
  OUT out_debtor_not_found BOOLEAN,
  OUT out_same_account BOOLEAN,
  OUT out_balance_insufficient BOOLEAN,
  OUT out_creditor_admin BOOLEAN,
  OUT out_tan_required BOOLEAN,
  OUT out_request_uid_reuse BOOLEAN,
  OUT out_bad_amount BOOLEAN,
  -- Success return
  OUT out_credit_bank_account_id INT8,
  OUT out_debit_bank_account_id INT8,
  OUT out_credit_row_id INT8,
  OUT out_debit_row_id INT8,
  OUT out_creditor_is_exchange BOOLEAN,
  OUT out_debtor_is_exchange BOOLEAN,
  OUT out_idempotent BOOLEAN
)
LANGUAGE plpgsql AS $$
BEGIN
-- Find credit bank account id and check it's not admin
SELECT bank_account_id, is_taler_exchange, username='admin'
  INTO out_credit_bank_account_id, out_creditor_is_exchange, out_creditor_admin
  FROM bank_accounts
    JOIN customers ON customer_id=owning_customer_id
  WHERE internal_payto = in_credit_account_payto AND deleted_at IS NULL;
IF NOT FOUND OR out_creditor_admin THEN
  out_creditor_not_found=NOT FOUND;
  RETURN;
END IF;
-- Find debit bank account ID and check it's a different account and if 2FA is required
SELECT bank_account_id, is_taler_exchange, out_credit_bank_account_id=bank_account_id, NOT in_is_tan AND tan_channel IS NOT NULL
  INTO out_debit_bank_account_id, out_debtor_is_exchange, out_same_account, out_tan_required
  FROM bank_accounts 
    JOIN customers ON customer_id=owning_customer_id
  WHERE username = in_debit_account_username AND deleted_at IS NULL;
IF NOT FOUND OR out_same_account THEN
  out_debtor_not_found=NOT FOUND;
  RETURN;
END IF;
-- Check for idempotence and conflict
IF in_request_uid IS NOT NULL THEN
  SELECT (amount != in_amount
      OR subject != in_subject 
      OR bank_account_id != out_debit_bank_account_id), bank_transaction
    INTO out_request_uid_reuse, out_debit_row_id
    FROM bank_transaction_operations
      JOIN bank_account_transactions ON bank_transaction = bank_transaction_id
    WHERE request_uid = in_request_uid;
  IF found OR out_tan_required THEN
    out_idempotent = found AND NOT out_request_uid_reuse;
    RETURN;
  END IF;
ELSIF out_tan_required THEN
  RETURN;
END IF;

-- Perform bank transfer
SELECT
  transfer.out_balance_insufficient,
  transfer.out_bad_amount,
  transfer.out_credit_row_id,
  transfer.out_debit_row_id
  INTO
    out_balance_insufficient,
    out_bad_amount,
    out_credit_row_id,
    out_debit_row_id
  FROM bank_wire_transfer(
    out_credit_bank_account_id,
    out_debit_bank_account_id,
    in_subject,
    in_amount,
    in_timestamp,
    in_wire_transfer_fees,
    in_min_amount,
    in_max_amount
  ) as transfer;
IF out_balance_insufficient OR out_bad_amount THEN
  RETURN;
END IF;

-- Store operation
IF in_request_uid IS NOT NULL THEN
  INSERT INTO bank_transaction_operations (request_uid, bank_transaction)  
    VALUES (in_request_uid, out_debit_row_id);
END IF;
END $$;
COMMENT ON FUNCTION bank_transaction IS 'Create a bank transaction';

CREATE FUNCTION create_taler_withdrawal(
  IN in_account_username TEXT,
  IN in_withdrawal_uuid UUID,
  IN in_amount taler_amount,
  IN in_suggested_amount taler_amount,
  IN in_no_amount_to_wallet BOOLEAN,
  IN in_timestamp INT8,
  IN in_wire_transfer_fees taler_amount,
  IN in_min_amount taler_amount,
  IN in_max_amount taler_amount,
   -- Error status
  OUT out_account_not_found BOOLEAN,
  OUT out_account_is_exchange BOOLEAN,
  OUT out_balance_insufficient BOOLEAN,
  OUT out_bad_amount BOOLEAN
)
LANGUAGE plpgsql AS $$ 
DECLARE
account_id INT8;
amount_with_fee taler_amount;
BEGIN
-- Check account exists
SELECT bank_account_id, is_taler_exchange
  INTO account_id, out_account_is_exchange
  FROM bank_accounts
  JOIN customers ON bank_accounts.owning_customer_id = customers.customer_id
  WHERE username=in_account_username AND deleted_at IS NULL;
IF NOT FOUND OR out_account_is_exchange THEN
  out_account_not_found=NOT FOUND;
  RETURN;
END IF;

-- Check enough funds
IF in_amount IS NOT NULL OR in_suggested_amount IS NOT NULL THEN
  SELECT test.out_balance_insufficient, test.out_bad_amount FROM account_balance_is_sufficient(
    account_id, 
    COALESCE(in_amount, in_suggested_amount), 
    in_wire_transfer_fees,
    in_min_amount,
    in_max_amount
  ) AS test INTO out_balance_insufficient, out_bad_amount;
  IF out_balance_insufficient OR out_bad_amount THEN
    RETURN;
  END IF;
END IF;

-- Create withdrawal operation
INSERT INTO taler_withdrawal_operations (
  withdrawal_uuid,
  wallet_bank_account,
  amount,
  suggested_amount,
  no_amount_to_wallet,
  creation_date
) VALUES (
  in_withdrawal_uuid,
  account_id,
  in_amount,
  in_suggested_amount,
  in_no_amount_to_wallet,
  in_timestamp
);
END $$;
COMMENT ON FUNCTION create_taler_withdrawal IS 'Create a new withdrawal operation';

CREATE FUNCTION select_taler_withdrawal(
  IN in_withdrawal_uuid uuid,
  IN in_reserve_pub BYTEA,
  IN in_subject TEXT,
  IN in_selected_exchange_payto TEXT,
  IN in_amount taler_amount,
  IN in_wire_transfer_fees taler_amount,
  IN in_min_amount taler_amount,
  IN in_max_amount taler_amount,
  -- Error status
  OUT out_no_op BOOLEAN,
  OUT out_already_selected BOOLEAN,
  OUT out_reserve_pub_reuse BOOLEAN,
  OUT out_account_not_found BOOLEAN,
  OUT out_account_is_not_exchange BOOLEAN,
  OUT out_amount_differs BOOLEAN,
  OUT out_balance_insufficient BOOLEAN,
  OUT out_bad_amount BOOLEAN,
  OUT out_aborted BOOLEAN,
  -- Success return
  OUT out_status TEXT
)
LANGUAGE plpgsql AS $$ 
DECLARE
not_selected BOOLEAN;
account_id int8;
amount_with_fee taler_amount;
BEGIN
-- Check for conflict and idempotence
SELECT
  NOT selection_done, 
  aborted,
  CASE 
    WHEN confirmation_done THEN 'confirmed'
    ELSE 'selected'
  END,
  selection_done 
    AND (selected_exchange_payto != in_selected_exchange_payto OR reserve_pub != in_reserve_pub OR amount != in_amount),
  amount != in_amount,
  wallet_bank_account
  INTO not_selected, out_aborted, out_status, out_already_selected, out_amount_differs, account_id
  FROM taler_withdrawal_operations
  WHERE withdrawal_uuid=in_withdrawal_uuid;
out_no_op = NOT FOUND;
IF out_no_op OR out_aborted OR out_already_selected OR out_amount_differs THEN
  RETURN;
END IF;

IF not_selected THEN
  -- Check reserve_pub reuse
  SELECT EXISTS(SELECT FROM taler_exchange_incoming WHERE metadata = in_reserve_pub AND type = 'reserve') OR
    EXISTS(SELECT FROM taler_withdrawal_operations WHERE reserve_pub = in_reserve_pub) 
    INTO out_reserve_pub_reuse;
  IF out_reserve_pub_reuse THEN
    RETURN;
  END IF;

  -- Check exchange account
  SELECT NOT is_taler_exchange
    INTO out_account_is_not_exchange
    FROM bank_accounts
    WHERE internal_payto=in_selected_exchange_payto;
  IF NOT FOUND OR out_account_is_not_exchange THEN
    out_account_not_found=NOT FOUND;
    RETURN;
  END IF;

  IF in_amount IS NOT NULL THEN
    SELECT test.out_balance_insufficient, test.out_bad_amount FROM account_balance_is_sufficient(
      account_id,
      in_amount,
      in_wire_transfer_fees,
      in_min_amount,
      in_max_amount
    ) AS test INTO out_balance_insufficient, out_bad_amount;
    IF out_balance_insufficient OR out_bad_amount THEN
      RETURN;
    END IF;
  END IF;

  -- Update withdrawal operation
  UPDATE taler_withdrawal_operations
    SET selected_exchange_payto=in_selected_exchange_payto,
        reserve_pub=in_reserve_pub,
        subject=in_subject,
        selection_done=true,
        amount=COALESCE(amount, in_amount)
    WHERE withdrawal_uuid=in_withdrawal_uuid;

  -- Notify status change
  PERFORM pg_notify('bank_withdrawal_status', in_withdrawal_uuid::text || ' selected');
END IF;
END $$;
COMMENT ON FUNCTION select_taler_withdrawal IS 'Set details of a withdrawal operation';

CREATE FUNCTION abort_taler_withdrawal(
  IN in_withdrawal_uuid uuid,
  IN in_username TEXT,
  OUT out_no_op BOOLEAN,
  OUT out_already_confirmed BOOLEAN
)
LANGUAGE plpgsql AS $$
BEGIN
UPDATE taler_withdrawal_operations
  SET aborted = NOT confirmation_done
  FROM bank_accounts
  JOIN customers ON owning_customer_id=customer_id
  WHERE withdrawal_uuid=in_withdrawal_uuid
    AND wallet_bank_account=bank_account_id
    AND (in_username IS NULL OR username = in_username)
  RETURNING confirmation_done
  INTO out_already_confirmed;
IF NOT FOUND OR out_already_confirmed THEN
  out_no_op=NOT FOUND;
  RETURN;
END IF;

-- Notify status change
PERFORM pg_notify('bank_withdrawal_status', in_withdrawal_uuid::text || ' aborted');
END $$;
COMMENT ON FUNCTION abort_taler_withdrawal IS 'Abort a withdrawal operation.';

CREATE FUNCTION confirm_taler_withdrawal(
  IN in_username TEXT,
  IN in_withdrawal_uuid uuid,
  IN in_timestamp INT8,
  IN in_is_tan BOOLEAN,
  IN in_wire_transfer_fees taler_amount,
  IN in_min_amount taler_amount,
  IN in_max_amount taler_amount,
  IN in_amount taler_amount,
  OUT out_no_op BOOLEAN,
  OUT out_balance_insufficient BOOLEAN,
  OUT out_bad_amount BOOLEAN,
  OUT out_creditor_not_found BOOLEAN,
  OUT out_exchange_not_found BOOLEAN,
  OUT out_not_selected BOOLEAN,
  OUT out_missing_amount BOOLEAN,
  OUT out_amount_differs BOOLEAN,
  OUT out_aborted BOOLEAN,
  OUT out_tan_required BOOLEAN
)
LANGUAGE plpgsql AS $$
DECLARE
  already_confirmed BOOLEAN;
  subject_local TEXT;
  reserve_pub_local BYTEA;
  selected_exchange_payto_local TEXT;
  wallet_bank_account_local INT8;
  amount_local taler_amount;
  exchange_bank_account_id INT8;
  tx_row_id INT8;
BEGIN
-- Check op exists and conflict
SELECT
  confirmation_done,
  aborted, NOT selection_done,
  reserve_pub, subject,
  selected_exchange_payto,
  wallet_bank_account,
  (amount).val, (amount).frac,
  (NOT in_is_tan AND tan_channel IS NOT NULL),
  amount IS NULL AND in_amount IS NULL,
  amount != in_amount
  INTO
    already_confirmed,
    out_aborted, out_not_selected,
    reserve_pub_local, subject_local,
    selected_exchange_payto_local,
    wallet_bank_account_local,
    amount_local.val, amount_local.frac,
    out_tan_required,
    out_missing_amount,
    out_amount_differs
  FROM taler_withdrawal_operations
    JOIN bank_accounts ON wallet_bank_account=bank_account_id
    JOIN customers ON owning_customer_id=customer_id
  WHERE withdrawal_uuid=in_withdrawal_uuid AND username=in_username AND deleted_at IS NULL;
out_no_op=NOT FOUND;
IF out_no_op OR already_confirmed OR out_aborted OR out_not_selected OR out_missing_amount OR out_amount_differs THEN
  RETURN;
ELSIF in_amount IS NOT NULL THEN
  amount_local = in_amount;
END IF;

-- Check exchange account then 2fa
SELECT
  bank_account_id
  INTO exchange_bank_account_id
  FROM bank_accounts
  WHERE internal_payto = selected_exchange_payto_local;
IF NOT FOUND OR out_tan_required THEN
  out_exchange_not_found=NOT FOUND;
  RETURN;
END IF;

SELECT -- not checking for accounts existence, as it was done above.
  transfer.out_balance_insufficient,
  transfer.out_bad_amount,
  out_credit_row_id
  INTO out_balance_insufficient, out_bad_amount, tx_row_id
FROM bank_wire_transfer(
  exchange_bank_account_id,
  wallet_bank_account_local,
  subject_local,
  amount_local,
  in_timestamp,
  in_wire_transfer_fees,
  in_min_amount,
  in_max_amount
) as transfer;
IF out_balance_insufficient OR out_bad_amount THEN
  RETURN;
END IF;

-- Confirm operation and update amount
UPDATE taler_withdrawal_operations
  SET amount=amount_local, confirmation_done = true
  WHERE withdrawal_uuid=in_withdrawal_uuid;

-- Register incoming transaction
CALL register_incoming(tx_row_id, 'reserve'::taler_incoming_type, reserve_pub_local, exchange_bank_account_id);

-- Notify status change
PERFORM pg_notify('bank_withdrawal_status', in_withdrawal_uuid::text || ' confirmed');
END $$;
COMMENT ON FUNCTION confirm_taler_withdrawal
  IS 'Set a withdrawal operation as confirmed and wire the funds to the exchange.';

CREATE FUNCTION cashin(
  IN in_timestamp INT8,
  IN in_reserve_pub BYTEA,
  IN in_amount taler_amount,
  IN in_subject TEXT,
  -- Error status
  OUT out_no_account BOOLEAN,
  OUT out_too_small BOOLEAN,
  OUT out_no_config BOOLEAN,
  OUT out_balance_insufficient BOOLEAN
)
LANGUAGE plpgsql AS $$ 
DECLARE
  converted_amount taler_amount;
  admin_account_id INT8;
  exchange_account_id INT8;
  tx_row_id INT8;
BEGIN
-- TODO check reserve_pub reuse ?

-- Recover exchange account info
SELECT bank_account_id
  INTO exchange_account_id
  FROM bank_accounts
    JOIN customers 
      ON customer_id=owning_customer_id
  WHERE username = 'exchange';
IF NOT FOUND THEN
  out_no_account = true;
  RETURN;
END IF;

-- Retrieve admin account id
SELECT bank_account_id
  INTO admin_account_id
  FROM bank_accounts
    JOIN customers 
      ON customer_id=owning_customer_id
  WHERE username = 'admin';

-- Perform conversion
SELECT (converted).val, (converted).frac, too_small, no_config
  INTO converted_amount.val, converted_amount.frac, out_too_small, out_no_config
  FROM conversion_to(in_amount, 'cashin'::text, null);
IF out_too_small OR out_no_config THEN
  RETURN;
END IF;

-- Perform bank wire transfer
SELECT 
  transfer.out_balance_insufficient,
  transfer.out_credit_row_id
  INTO 
    out_balance_insufficient,
    tx_row_id
  FROM bank_wire_transfer(
    exchange_account_id,
    admin_account_id,
    in_subject,
    converted_amount,
    in_timestamp,
    NULL,
    NULL,
    NULL
  ) as transfer;
IF out_balance_insufficient THEN
  RETURN;
END IF;

-- Register incoming transaction
CALL register_incoming(tx_row_id, 'reserve'::taler_incoming_type, in_reserve_pub, exchange_account_id);

-- update stats
CALL stats_register_payment('cashin', NULL, converted_amount, in_amount);

END $$;
COMMENT ON FUNCTION cashin IS 'Perform a cashin operation';


CREATE FUNCTION cashout_create(
  IN in_username TEXT,
  IN in_request_uid BYTEA,
  IN in_amount_debit taler_amount,
  IN in_amount_credit taler_amount,
  IN in_subject TEXT,
  IN in_timestamp INT8,
  IN in_is_tan BOOLEAN,
  -- Error status
  OUT out_bad_conversion BOOLEAN,
  OUT out_account_not_found BOOLEAN,
  OUT out_account_is_exchange BOOLEAN,
  OUT out_balance_insufficient BOOLEAN,
  OUT out_request_uid_reuse BOOLEAN,
  OUT out_no_cashout_payto BOOLEAN,
  OUT out_tan_required BOOLEAN,
  OUT out_under_min BOOLEAN,
  -- Success return
  OUT out_cashout_id INT8
)
LANGUAGE plpgsql AS $$ 
DECLARE
account_id INT8;
admin_account_id INT8;
tx_id INT8;
custom_min_cashout taler_amount;
BEGIN

-- Check account exists, has all info and if 2FA is required
SELECT 
    bank_account_id, is_taler_exchange, 
    (min_cashout).val, (min_cashout).frac,
    cashout_payto IS NULL, (NOT in_is_tan AND tan_channel IS NOT NULL) 
  INTO 
    account_id, out_account_is_exchange, 
    custom_min_cashout.val, custom_min_cashout.frac,
    out_no_cashout_payto, out_tan_required
  FROM bank_accounts
  JOIN customers ON owning_customer_id=customer_id
  WHERE username=in_username;
IF NOT FOUND THEN
  out_account_not_found=TRUE;
  RETURN;
ELSIF out_account_is_exchange OR out_no_cashout_payto THEN
  RETURN;
END IF;

-- check conversion
IF custom_min_cashout.val IS NULL THEN
  custom_min_cashout = NULL;
END IF;
SELECT under_min, too_small OR no_config OR in_amount_credit!=converted
  INTO out_under_min, out_bad_conversion 
  FROM conversion_to(in_amount_debit, 'cashout'::text, custom_min_cashout);
IF out_bad_conversion THEN
  RETURN;
END IF;

-- Retrieve admin account id
SELECT bank_account_id
  INTO admin_account_id
  FROM bank_accounts
    JOIN customers 
      ON customer_id=owning_customer_id
  WHERE username = 'admin';

-- Check for idempotence and conflict
SELECT (amount_debit != in_amount_debit
          OR subject != in_subject 
          OR bank_account != account_id)
        , cashout_id
  INTO out_request_uid_reuse, out_cashout_id
  FROM cashout_operations
  WHERE request_uid = in_request_uid;
IF found OR out_request_uid_reuse OR out_tan_required THEN
  RETURN;
END IF;

-- Perform bank wire transfer
SELECT transfer.out_balance_insufficient, out_debit_row_id
INTO out_balance_insufficient, tx_id
FROM bank_wire_transfer(
  admin_account_id,
  account_id,
  in_subject,
  in_amount_debit,
  in_timestamp,
  NULL,
  NULL,
  NULL
) as transfer;
IF out_balance_insufficient THEN
  RETURN;
END IF;

-- Create cashout operation
INSERT INTO cashout_operations (
  request_uid
  ,amount_debit
  ,amount_credit
  ,creation_time
  ,bank_account
  ,subject
  ,local_transaction
) VALUES (
  in_request_uid
  ,in_amount_debit
  ,in_amount_credit
  ,in_timestamp
  ,account_id
  ,in_subject
  ,tx_id
) RETURNING cashout_id INTO out_cashout_id;

-- update stats
CALL stats_register_payment('cashout', NULL, in_amount_debit, in_amount_credit);
END $$;

CREATE FUNCTION tan_challenge_create (
  IN in_body TEXT,
  IN in_op op_enum,
  IN in_code TEXT,
  IN in_timestamp INT8,
  IN in_validity_period INT8,
  IN in_retry_counter INT4,
  IN in_username TEXT,
  IN in_tan_channel tan_enum,
  IN in_tan_info TEXT,
  OUT out_challenge_id INT8
)
LANGUAGE plpgsql as $$
DECLARE
account_id INT8;
BEGIN
-- Retrieve account id
SELECT customer_id INTO account_id FROM customers WHERE username = in_username AND deleted_at IS NULL;
-- Create challenge
INSERT INTO tan_challenges (
  body,
  op,
  code,
  creation_date,
  expiration_date,
  retry_counter,
  customer,
  tan_channel,
  tan_info
) VALUES (
  in_body,
  in_op,
  in_code,
  in_timestamp,
  in_timestamp + in_validity_period,
  in_retry_counter,
  account_id,
  in_tan_channel,
  in_tan_info
) RETURNING challenge_id INTO out_challenge_id;
END $$;
COMMENT ON FUNCTION tan_challenge_create IS 'Create a new challenge, return the generated id';

CREATE FUNCTION tan_challenge_send(
  IN in_challenge_id INT8,
  IN in_username TEXT,
  IN in_code TEXT,              -- New code to use if the old code expired
  IN in_timestamp INT8,        
  IN in_validity_period INT8,
  IN in_retry_counter INT4,
  IN in_auth BOOLEAN,
  IN in_max_active INT,
  -- Error status
  OUT out_no_op BOOLEAN,
  OUT out_auth_required BOOLEAN,
  OUT out_too_many BOOLEAN,
  -- Success return
  OUT out_tan_code TEXT,        -- TAN code to send, NULL if nothing should be sent
  OUT out_tan_channel tan_enum, -- TAN channel to use, NULL if nothing should be sent
  OUT out_tan_info TEXT         -- TAN info to use, NULL if nothing should be sent
)
LANGUAGE plpgsql as $$
DECLARE
account_id INT8;
expired BOOLEAN;
retransmit BOOLEAN;
BEGIN
-- Retrieve account id
SELECT customer_id, tan_channel, CASE tan_channel
    WHEN 'sms'   THEN phone
    WHEN 'email' THEN email
  END
INTO account_id, out_tan_channel, out_tan_info
FROM customers WHERE username = in_username AND deleted_at IS NULL;

-- Recover expiration date
SELECT 
  (in_timestamp >= expiration_date OR 0 >= retry_counter) AND confirmation_date IS NULL
  ,in_timestamp >= retransmission_date AND confirmation_date IS NULL
  ,NOT in_auth AND op != 'create_token'
  ,code
  ,COALESCE(tan_channel, out_tan_channel)
  ,COALESCE(tan_info, out_tan_info)
  -- If this is the first time we submit this challenge check there is not too many active challenges
  ,retransmission_date = 0 AND (
    SELECT count(*) >= in_max_active
    FROM tan_challenges 
    WHERE customer = account_id
      AND retransmission_date != 0
      AND confirmation_date IS NULL
      AND expiration_date >= in_timestamp
  )
INTO expired, retransmit, out_auth_required, out_tan_code, out_tan_channel, out_tan_info, out_too_many
FROM tan_challenges WHERE challenge_id = in_challenge_id AND customer = account_id;
out_no_op = NOT FOUND;
IF out_no_op OR out_auth_required THEN
  IF out_no_op AND NOT in_auth THEN
    out_no_op=false;
    out_auth_required=true;
  END IF;
  RETURN;
END IF;

IF expired THEN
  UPDATE tan_challenges SET
     code = in_code
    ,expiration_date = in_timestamp + in_validity_period
    ,retry_counter = in_retry_counter
  WHERE challenge_id = in_challenge_id;
  out_tan_code = in_code;
ELSIF NOT retransmit THEN
  out_tan_code = NULL;
END IF;
END $$;
COMMENT ON FUNCTION tan_challenge_send IS 'Get the challenge to send, return NULL if nothing should be sent';

CREATE FUNCTION tan_challenge_mark_sent (
  IN in_challenge_id INT8,
  IN in_timestamp INT8,
  IN in_retransmission_period INT8
) RETURNS void
LANGUAGE sql AS $$
  UPDATE tan_challenges SET 
    retransmission_date = in_timestamp + in_retransmission_period
  WHERE challenge_id = in_challenge_id;
$$;
COMMENT ON FUNCTION tan_challenge_mark_sent IS 'Register a challenge as successfully sent';

CREATE FUNCTION tan_challenge_try (
  IN in_challenge_id INT8, 
  IN in_username TEXT,
  IN in_code TEXT,    
  IN in_timestamp INT8,
  IN in_auth BOOLEAN,
  -- Error status       
  OUT out_ok BOOLEAN,
  OUT out_no_op BOOLEAN,
  OUT out_no_retry BOOLEAN,
  OUT out_expired BOOLEAN,
  OUT out_auth_required BOOLEAN,
  -- Success return
  OUT out_op op_enum,
  OUT out_body TEXT,
  OUT out_channel tan_enum,
  OUT out_info TEXT
)
LANGUAGE plpgsql as $$
DECLARE
account_id INT8;
token_creation BOOLEAN;
BEGIN
-- Retrieve account id
SELECT customer_id INTO account_id FROM customers WHERE username = in_username AND deleted_at IS NULL;

-- Check if challenge exist and if auth is required
SELECT NOT in_auth AND op != 'create_token', op = 'create_token'
INTO out_auth_required, token_creation
FROM tan_challenges
WHERE challenge_id = in_challenge_id AND customer = account_id;
out_no_op = NOT FOUND;
IF out_no_op OR out_auth_required THEN
  IF out_no_op AND NOT in_auth THEN
    out_no_op=false;
    out_auth_required=true;
  END IF;
  RETURN;
END IF;

-- Try to solve challenge
UPDATE tan_challenges SET 
  confirmation_date = CASE 
    WHEN (retry_counter > 0 AND in_timestamp < expiration_date AND code = in_code) THEN in_timestamp
    ELSE confirmation_date
  END,
  retry_counter = retry_counter - 1
WHERE challenge_id = in_challenge_id
RETURNING 
  confirmation_date IS NOT NULL, 
  retry_counter <= 0 AND confirmation_date IS NULL,
  in_timestamp >= expiration_date AND confirmation_date IS NULL
INTO out_ok, out_no_retry, out_expired;
out_no_op = NOT FOUND;

IF NOT out_ok AND token_creation THEN
  UPDATE customers SET token_creation_counter=token_creation_counter+1 WHERE customer_id=account_id;
END IF;

IF out_no_op OR NOT out_ok OR out_no_retry OR out_expired THEN
  RETURN;
END IF;

-- Recover body and op from challenge
SELECT body, op, tan_channel, tan_info
  INTO out_body, out_op, out_channel, out_info
  FROM tan_challenges WHERE challenge_id = in_challenge_id;
END $$;
COMMENT ON FUNCTION tan_challenge_try IS 'Try to confirm a challenge, return true if the challenge have been confirmed';

CREATE FUNCTION stats_get_frame(
  IN date TIMESTAMP,
  IN in_timeframe stat_timeframe_enum,
  OUT cashin_count INT8,
  OUT cashin_regional_volume taler_amount,
  OUT cashin_fiat_volume taler_amount,
  OUT cashout_count INT8,
  OUT cashout_regional_volume taler_amount,
  OUT cashout_fiat_volume taler_amount,
  OUT taler_in_count INT8,
  OUT taler_in_volume taler_amount,
  OUT taler_out_count INT8,
  OUT taler_out_volume taler_amount
)
LANGUAGE plpgsql AS $$
BEGIN
  date = date_trunc(in_timeframe::text, date);
  SELECT 
    s.cashin_count
    ,(s.cashin_regional_volume).val
    ,(s.cashin_regional_volume).frac
    ,(s.cashin_fiat_volume).val
    ,(s.cashin_fiat_volume).frac
    ,s.cashout_count
    ,(s.cashout_regional_volume).val
    ,(s.cashout_regional_volume).frac
    ,(s.cashout_fiat_volume).val
    ,(s.cashout_fiat_volume).frac
    ,s.taler_in_count
    ,(s.taler_in_volume).val
    ,(s.taler_in_volume).frac
    ,s.taler_out_count
    ,(s.taler_out_volume).val
    ,(s.taler_out_volume).frac
  INTO
    cashin_count
    ,cashin_regional_volume.val
    ,cashin_regional_volume.frac
    ,cashin_fiat_volume.val
    ,cashin_fiat_volume.frac
    ,cashout_count
    ,cashout_regional_volume.val
    ,cashout_regional_volume.frac
    ,cashout_fiat_volume.val
    ,cashout_fiat_volume.frac
    ,taler_in_count
    ,taler_in_volume.val
    ,taler_in_volume.frac
    ,taler_out_count
    ,taler_out_volume.val
    ,taler_out_volume.frac
  FROM bank_stats AS s
  WHERE s.timeframe = in_timeframe 
    AND s.start_time = date;
END $$;

CREATE PROCEDURE stats_register_payment(
  IN name TEXT,
  IN now TIMESTAMP,
  IN regional_amount taler_amount,
  IN fiat_amount taler_amount
)
LANGUAGE plpgsql AS $$
BEGIN
  IF now IS NULL THEN
    now = timezone('utc', now())::TIMESTAMP;
  END IF;
  IF name = 'taler_in' THEN
    INSERT INTO bank_stats AS s (
      timeframe,
      start_time,
      taler_in_count,
      taler_in_volume
    ) SELECT
      frame,
      date_trunc(frame::text, now),
      1,
      regional_amount
    FROM unnest(enum_range(null::stat_timeframe_enum)) AS frame
    ON CONFLICT (timeframe, start_time) DO UPDATE 
    SET taler_in_count=s.taler_in_count+1,
        taler_in_volume=(SELECT amount_add(s.taler_in_volume, regional_amount));
  ELSIF name = 'taler_out' THEN
    INSERT INTO bank_stats AS s (
      timeframe,
      start_time,
      taler_out_count,
      taler_out_volume
    ) SELECT
      frame,
      date_trunc(frame::text, now),
      1,
      regional_amount
    FROM unnest(enum_range(null::stat_timeframe_enum)) AS frame
    ON CONFLICT (timeframe, start_time) DO UPDATE 
    SET taler_out_count=s.taler_out_count+1,
        taler_out_volume=(SELECT amount_add(s.taler_out_volume, regional_amount));
  ELSIF name = 'cashin' THEN
    INSERT INTO bank_stats AS s (
      timeframe,
      start_time,
      cashin_count,
      cashin_regional_volume,
      cashin_fiat_volume
    ) SELECT
      frame,
      date_trunc(frame::text, now),
      1,
      regional_amount,
      fiat_amount
    FROM unnest(enum_range(null::stat_timeframe_enum)) AS frame 
    ON CONFLICT (timeframe, start_time) DO UPDATE 
    SET cashin_count=s.cashin_count+1,
        cashin_regional_volume=(SELECT amount_add(s.cashin_regional_volume, regional_amount)),
        cashin_fiat_volume=(SELECT amount_add(s.cashin_fiat_volume, fiat_amount));
  ELSIF name = 'cashout' THEN
    INSERT INTO bank_stats AS s (
      timeframe,
      start_time,
      cashout_count,
      cashout_regional_volume,
      cashout_fiat_volume
    ) SELECT
      frame,
      date_trunc(frame::text, now),
      1,
      regional_amount,
      fiat_amount
    FROM unnest(enum_range(null::stat_timeframe_enum)) AS frame 
    ON CONFLICT (timeframe, start_time) DO UPDATE 
    SET cashout_count=s.cashout_count+1,
        cashout_regional_volume=(SELECT amount_add(s.cashout_regional_volume, regional_amount)),
        cashout_fiat_volume=(SELECT amount_add(s.cashout_fiat_volume, fiat_amount));
  ELSE
    RAISE EXCEPTION 'Unknown stat %', name;
  END IF;
END $$;

CREATE PROCEDURE config_set_amount(
  IN name TEXT,
  IN amount taler_amount
)
LANGUAGE sql AS $$
  INSERT INTO config (key, value) VALUES (name, jsonb_build_object('val', amount.val, 'frac', amount.frac))
    ON CONFLICT (key) DO UPDATE SET value = excluded.value
$$;

CREATE PROCEDURE config_set_rounding_mode(
  IN name TEXT,
  IN mode rounding_mode
)
LANGUAGE sql AS $$
  INSERT INTO config (key, value) VALUES (name, jsonb_build_object('mode', mode::text))
    ON CONFLICT (key) DO UPDATE SET value = excluded.value
$$;

CREATE FUNCTION config_get_amount(
  IN name TEXT,
  OUT amount taler_amount
)
LANGUAGE sql AS $$
  SELECT (value['val']::int8, value['frac']::int4)::taler_amount FROM config WHERE key=name
$$;

CREATE FUNCTION config_get_rounding_mode(
  IN name TEXT,
  OUT mode rounding_mode
)
LANGUAGE sql AS $$ SELECT (value->>'mode')::rounding_mode FROM config WHERE key=name $$;

CREATE FUNCTION conversion_apply_ratio(
   IN amount taler_amount
  ,IN ratio taler_amount
  ,IN fee taler_amount
  ,IN tiny taler_amount       -- Result is rounded to this amount
  ,IN rounding rounding_mode  -- With this rounding mode
  ,OUT result taler_amount
  ,OUT out_too_small BOOLEAN
)
LANGUAGE plpgsql AS $$
DECLARE
  amount_numeric NUMERIC(33, 8); -- 16 digit for val, 8 for frac and 1 for rounding error
  tiny_numeric NUMERIC(24);
  rounding_error real;
BEGIN
  -- Perform multiplication using big numbers
  amount_numeric = (amount.val::numeric(24) * 100000000 + amount.frac::numeric(24)) * (ratio.val::numeric(24, 8) + ratio.frac::numeric(24, 8) / 100000000);

  -- Apply fees
  amount_numeric = amount_numeric - (fee.val::numeric(24) * 100000000 + fee.frac::numeric(24));
  IF (sign(amount_numeric) != 1) THEN
    out_too_small = TRUE;
    result = (0, 0);
    RETURN;
  END IF;

  -- Round to tiny amounts
  tiny_numeric = (tiny.val::numeric(24) * 100000000 + tiny.frac::numeric(24));
  amount_numeric = amount_numeric / tiny_numeric;
  rounding_error = (amount_numeric % 1)::real;
  amount_numeric = trunc(amount_numeric) * tiny_numeric;
  
  -- Apply rounding mode
  IF (rounding = 'nearest'::rounding_mode AND rounding_error >= 0.5)
    OR (rounding = 'up'::rounding_mode AND rounding_error > 0.0) THEN
    amount_numeric = amount_numeric + tiny_numeric;
  END IF;

  -- Extract product parts
  result = (trunc(amount_numeric / 100000000)::int8, (amount_numeric % 100000000)::int4);

  IF (result.val > 1::INT8<<52) THEN
    RAISE EXCEPTION 'amount value overflowed';
  END IF;
END $$;
COMMENT ON FUNCTION conversion_apply_ratio
  IS 'Apply a ratio to an amount rounding the result to a tiny amount following a rounding mode. It raises an exception when the resulting .val is larger than 2^52';

CREATE FUNCTION conversion_revert_ratio(
   IN amount taler_amount
  ,IN ratio taler_amount
  ,IN fee taler_amount
  ,IN tiny taler_amount       -- Result is rounded to this amount
  ,IN rounding rounding_mode  -- With this rounding mode
  ,OUT result taler_amount
  ,OUT bad_value BOOLEAN
)
LANGUAGE plpgsql AS $$
DECLARE
  amount_numeric NUMERIC(33, 8); -- 16 digit for val, 8 for frac and 1 for rounding error
  tiny_numeric NUMERIC(24);
  rounding_error real;
BEGIN
  -- Apply fees
  amount_numeric = (amount.val::numeric(24) * 100000000 + amount.frac::numeric(24))  + (fee.val::numeric(24) * 100000000 + fee.frac::numeric(24));

  -- Perform division using big numbers
  amount_numeric = amount_numeric / (ratio.val::numeric(24, 8) + ratio.frac::numeric(24, 8) / 100000000);

  -- Round to tiny amounts
  tiny_numeric = (tiny.val::numeric(24) * 100000000 - tiny.frac::numeric(24));
  amount_numeric = amount_numeric / tiny_numeric;
  rounding_error = (amount_numeric % 1)::real;
  amount_numeric = trunc(amount_numeric) * tiny_numeric;

  -- Recover potentially lost tiny amount during rounding
  IF (rounding = 'zero'::rounding_mode AND rounding_error > 0.0) THEN
    amount_numeric = amount_numeric + tiny_numeric;
  END IF;

  -- Extract division parts
  result = (trunc(amount_numeric / 100000000)::int8, (amount_numeric % 100000000)::int4);

  IF (result.val > 1::INT8<<52) THEN
    RAISE EXCEPTION 'amount value overflowed';
  END IF;
END $$;
COMMENT ON FUNCTION conversion_revert_ratio
  IS 'Revert the application of a ratio. This function does not always return the smallest possible amount. It raises an exception when the resulting .val is larger than 2^52';


CREATE FUNCTION conversion_to(
  IN amount taler_amount,
  IN direction TEXT,
  IN custom_min_amount taler_amount,
  OUT converted taler_amount,
  OUT too_small BOOLEAN,
  OUT under_min BOOLEAN,
  OUT no_config BOOLEAN
)
LANGUAGE plpgsql AS $$
DECLARE
  at_ratio taler_amount;
  out_fee taler_amount;
  tiny_amount taler_amount;
  min_amount taler_amount;
  mode rounding_mode;
BEGIN
  -- Check min amount
  SELECT value['val']::int8, value['frac']::int4 INTO min_amount.val, min_amount.frac FROM config WHERE key=direction||'_min_amount';
  IF NOT FOUND THEN
    no_config = true;
    RETURN;
  END IF;
  IF custom_min_amount IS NOT NULL THEN
    min_amount = custom_min_amount;
  END IF;
 
  SELECT NOT ok INTO too_small FROM amount_left_minus_right(amount, min_amount);
  IF too_small THEN
    under_min = true;
    converted = (0, 0);
    RETURN;
  END IF;

  -- Perform conversion
  SELECT value['val']::int8, value['frac']::int4 INTO at_ratio.val, at_ratio.frac FROM config WHERE key=direction||'_ratio';
  SELECT value['val']::int8, value['frac']::int4 INTO out_fee.val, out_fee.frac FROM config WHERE key=direction||'_fee';
  SELECT value['val']::int8, value['frac']::int4 INTO tiny_amount.val, tiny_amount.frac FROM config WHERE key=direction||'_tiny_amount';
  SELECT (value->>'mode')::rounding_mode INTO mode FROM config WHERE key=direction||'_rounding_mode';

  SELECT (result).val, (result).frac, out_too_small INTO converted.val, converted.frac, too_small 
    FROM conversion_apply_ratio(amount, at_ratio, out_fee, tiny_amount, mode);
END $$;

CREATE FUNCTION conversion_from(
  IN amount taler_amount,
  IN direction TEXT,
  IN custom_min_amount taler_amount,
  OUT converted taler_amount,
  OUT too_small BOOLEAN,
  OUT under_min BOOLEAN,
  OUT no_config BOOLEAN
)
LANGUAGE plpgsql AS $$
DECLARE
  at_ratio taler_amount;
  out_fee taler_amount;
  tiny_amount taler_amount;
  min_amount taler_amount;
  mode rounding_mode;
BEGIN
  -- Perform conversion
  SELECT value['val']::int8, value['frac']::int4 INTO at_ratio.val, at_ratio.frac FROM config WHERE key=direction||'_ratio';
  SELECT value['val']::int8, value['frac']::int4 INTO out_fee.val, out_fee.frac FROM config WHERE key=direction||'_fee';
  SELECT value['val']::int8, value['frac']::int4 INTO tiny_amount.val, tiny_amount.frac FROM config WHERE key=direction||'_tiny_amount';
  SELECT (value->>'mode')::rounding_mode INTO mode FROM config WHERE key=direction||'_rounding_mode';
  IF NOT FOUND THEN
    no_config = true;
    RETURN;
  END IF;
  SELECT (result).val, (result).frac INTO converted.val, converted.frac 
    FROM conversion_revert_ratio(amount, at_ratio, out_fee, tiny_amount, mode);
  
  -- Check min amount
  SELECT value['val']::int8, value['frac']::int4 INTO min_amount.val, min_amount.frac FROM config WHERE key=direction||'_min_amount';
  IF custom_min_amount IS NOT NULL THEN
    min_amount = custom_min_amount;
  END IF;
  SELECT NOT ok INTO too_small FROM amount_left_minus_right(converted, min_amount);
  IF too_small THEN
    under_min = true;
    converted = (0, 0);
  END IF;
END $$;

COMMIT;