--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

BEGIN;

SELECT _v.register_patch('libeufin-bank-0008', NULL, NULL);
SET search_path TO libeufin_bank;

ALTER TABLE customers RENAME COLUMN login TO username;
ALTER TABLE bank_accounts RENAME internal_payto_uri TO internal_payto;
ALTER TABLE bank_account_transactions RENAME debtor_payto_uri TO debtor_payto;
ALTER TABLE bank_account_transactions RENAME creditor_payto_uri TO creditor_payto;
ALTER TABLE bank_account_transactions 
    DROP COLUMN account_servicer_reference,
    DROP COLUMN payment_information_id,
    DROP COLUMN end_to_end_id;
COMMIT;
