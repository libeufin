--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

BEGIN;

SELECT _v.register_patch('libeufin-bank-0006', NULL, NULL);
SET search_path TO libeufin_bank;

-- Add missing index for common queries

CREATE INDEX bank_accounts_public_index ON bank_accounts (bank_account_id) WHERE is_public = true;
COMMENT ON INDEX bank_accounts_public_index IS 'for listing public accounts';

CREATE INDEX bank_account_transactions_index ON bank_account_transactions (bank_account_id, bank_transaction_id);
COMMENT ON INDEX bank_accounts_public_index IS 'for listing bank account''s transaction';

CREATE INDEX bearer_tokens_index ON bearer_tokens USING btree (bank_customer, bearer_token_id);
COMMENT ON INDEX bearer_tokens_index IS 'for listing bank customer''s bearer token';

CREATE INDEX cashout_operations_index ON cashout_operations USING btree (bank_account, cashout_id);
COMMENT ON INDEX cashout_operations_index IS 'for listing bank customer''s cashout operations';

CREATE INDEX customers_deleted_index ON customers (customer_id) WHERE deleted_at IS NOT NULL;
COMMENT ON INDEX customers_deleted_index IS 'for garbage collection';

COMMIT;
