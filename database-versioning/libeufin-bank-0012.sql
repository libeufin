--
-- This file is part of TALER
-- Copyright (C) 2025 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

BEGIN;

SELECT _v.register_patch('libeufin-bank-0012', NULL, NULL);
SET search_path TO libeufin_bank;

-- Add no_amount_to_wallet for withdrawal
ALTER TABLE taler_withdrawal_operations ADD COLUMN no_amount_to_wallet BOOLEAN DEFAULT false;

-- Better polymorphism schema
ALTER TABLE taler_exchange_incoming ADD COLUMN metadata BYTEA;
UPDATE taler_exchange_incoming SET metadata=COALESCE(reserve_pub, account_pub, wad_id);
ALTER TABLE taler_exchange_incoming
  DROP CONSTRAINT incoming_polymorphism,
  DROP COLUMN reserve_pub,
  DROP COLUMN account_pub,
  DROP COLUMN wad_id,
  ALTER COLUMN metadata SET NOT NULL,
  ADD CONSTRAINT polymorphism CHECK(
    CASE type
      WHEN 'wad' THEN LENGTH(metadata)=24 AND origin_exchange_url IS NOT NULL
      ELSE LENGTH(metadata)=32 AND origin_exchange_url IS NULL
    END
  );
CREATE UNIQUE INDEX taler_exchange_incoming_unique_reserve_pub ON taler_exchange_incoming (metadata) WHERE type = 'reserve';

COMMIT;
