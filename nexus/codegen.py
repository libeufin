#!/usr/bin/python3
# Update EBICS constants file using latest external code sets files

from io import BytesIO
from zipfile import ZipFile

import polars as pl
import requests


def iso20022codegenExternalCodeSet():
    # Get XLSX zip file from server
    r = requests.get(
        "https://www.iso20022.org/sites/default/files/media/file/ExternalCodeSets_XLSX.zip"
    )
    assert r.status_code == 200

    # Unzip the XLSX file
    zip = ZipFile(BytesIO(r.content))
    files = zip.namelist()
    assert len(files) == 1
    bytes = zip.read(files[0])

    # Parse excel
    df = pl.read_excel(bytes, sheet_name="AllCodeSets")

    def extractCodeSet(setName: str, className: str) -> str:
        out = f"enum class {className}(val isoCode: String, val description: String) {{"

        for row in (
            df.filter(pl.col("Code Set") == setName).sort("Code Value").rows(named=True)
        ):
            (value, isoCode, description) = (
                row["Code Value"],
                row["Code Name"],
                row["Code Definition"].split("\n", 1)[0].strip().replace("_x000D_", ""),
            )
            out += f'\n\t{value}("{isoCode}", "{description}"),'

        out += "\n}"
        return out

    # Write kotlin file
    kt = f"""/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

// THIS FILE IS GENERATED, DO NOT EDIT

package tech.libeufin.nexus.iso20022

{extractCodeSet("ExternalStatusReason1Code", "ExternalStatusReasonCode")}

{extractCodeSet("ExternalPaymentGroupStatus1Code", "ExternalPaymentGroupStatusCode")}

{extractCodeSet("ExternalPaymentTransactionStatus1Code", "ExternalPaymentTransactionStatusCode")}

{extractCodeSet("ExternalReturnReason1Code", "ExternalReturnReasonCode")}

"""
    with open(
        "src/main/kotlin/tech/libeufin/nexus/iso20022/ExternalCodeSets.kt", "w"
    ) as file1:
        file1.write(kt)


def iso20022codegenBankTransactionCode():
    # Get XLSX zip file from server
    r = requests.get(
        "https://www.iso20022.org/sites/default/files/media/file/BTC_Codification_21March2024.xlsx"
    )
    assert r.status_code == 200

    # Get the XLSX file
    bytes = r.content

    # Parse excel
    df = pl.read_excel(
        bytes,
        sheet_name="BTC_Codification",
        read_options={"header_row": 2},
    ).rename(lambda name: name.splitlines()[0])

    def extractCodeSet(setName: str, className: str) -> str:
        out = f"enum class {className}(val description: String) {{"
        codeName = f"{setName} Code"
        for row in (
            df.group_by(codeName)
            .agg(pl.col(setName).unique().sort())
            .sort(codeName)
            .rows(named=True)
        ):
            if len(row[setName]) > 1:
                print(row)

            (code, description) = (
                row[codeName].strip().replace("\xa0", ""),
                row[setName][0].strip(),
            )
            out += f'\n\t{code}("{description}"),'

        out += "\n}"
        return out

    # Write kotlin file
    kt = f"""/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

// THIS FILE IS GENERATED, DO NOT EDIT

package tech.libeufin.nexus.iso20022

{extractCodeSet("Domain", "ExternalBankTransactionDomainCode")}

{extractCodeSet("Family", "ExternalBankTransactionFamilyCode")}

{extractCodeSet("SubFamily", "ExternalBankTransactionSubFamilyCode")}

"""
    with open(
        "src/main/kotlin/tech/libeufin/nexus/iso20022/BankTransactionCode.kt", "w"
    ) as file1:
        file1.write(kt)


iso20022codegenExternalCodeSet()
iso20022codegenBankTransactionCode()
