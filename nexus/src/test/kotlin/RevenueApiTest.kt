/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024-2025 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

import io.ktor.http.*
import io.ktor.client.request.*
import org.junit.Test
import tech.libeufin.common.RevenueIncomingHistory
import tech.libeufin.common.assertNotImplemented
import tech.libeufin.common.assertOk

class RevenueApiTest {
    // GET /taler-revenue/config
    @Test
    fun config() = serverSetup {
        client.get("/taler-revenue/config").assertOk()
    }

    // GET /taler-revenue/history
    @Test
    fun history() = serverSetup { db ->
        authRoutine(HttpMethod.Get, "/taler-revenue/history")

        historyRoutine<RevenueIncomingHistory>(
            url = "/taler-revenue/history",
            ids = { it.incoming_transactions.map { it.row_id } },
            registered = listOf(
                // Transactions using clean transfer logic
                { talerableIn(db) },
                { talerableCompletedIn(db) },

                // Common credit transactions
                { registerIn(db) },
                { registerCompletedIn(db) }
            ),
            ignored = listOf(
                // Ignore debit transactions
                { talerableOut(db) },

                // Incomplete taler
                { talerableIncompleteIn(db) },
                
                // Ignore incomplete
                { registerIncompleteIn(db) }
            )
        )
    }

    @Test
    fun noApi() = serverSetup("mini.conf") {
        client.getA("/taler-revenue/config").assertNotImplemented()
    }

    @Test
    fun auth() = serverSetup("auth.conf") {
        authRoutine(HttpMethod.Get, "/taler-revenue/history")
    }
}