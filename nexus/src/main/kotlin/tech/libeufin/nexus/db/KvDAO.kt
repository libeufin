/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.nexus.db

import tech.libeufin.common.*
import tech.libeufin.common.db.*
import tech.libeufin.nexus.iso20022.IncomingPayment
import tech.libeufin.nexus.iso20022.OutgoingPayment
import java.sql.Types
import java.time.Instant
import kotlinx.serialization.Contextual
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encodeToString
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule

object InstantSerialize : KSerializer<Instant> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("Instant", PrimitiveKind.LONG)
    override fun serialize(encoder: Encoder, value: Instant) =
        encoder.encodeLong(value.micros())
    override fun deserialize(decoder: Decoder): Instant =
        decoder.decodeLong().asInstant()
}

val JSON = Json {
    this.serializersModule = SerializersModule {
        contextual(Instant::class) { InstantSerialize }
    }
}

/** Data access logic for key value */
class KvDAO( val db: Database) {
    /** Get current value for [key] */
    suspend inline fun <reified T> get(key: String): T? = db.serializable(
        "SELECT value FROM kv WHERE key=?"
    ) {
        setString(1, key)
        oneOrNull {
            val encoded = it.getString(1)
            JSON.decodeFromString<T>(encoded)
        }
    }

    /** Set [value] for [key] */
    suspend inline fun <reified T> set(key: String, value: T) = db.serializable(
        "INSERT INTO kv (key, value) VALUES (?, ?::jsonb) ON CONFLICT (key) DO UPDATE SET value=EXCLUDED.value"
    ) {
        val encoded = JSON.encodeToString<T>(value)
        setString(1, key)
        setString(2, encoded)
        execute()
    }
}