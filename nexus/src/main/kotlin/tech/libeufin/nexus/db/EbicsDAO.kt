/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.nexus.db

import tech.libeufin.common.db.oneOrNull

/** Data access logic for EBICS transaction */
class EbicsDAO(private val db: Database) {
    /** Register a pending transaction */
    suspend fun register(id: String) = db.serializable(
        "INSERT INTO pending_ebics_transactions (tx_id) VALUES (?) ON CONFLICT DO NOTHING"
    ) {
        setString(1, id)
        executeUpdate()
    }

    /** Remove pending transaction */
    suspend fun remove(id: String) = db.serializable(
        "DELETE FROM pending_ebics_transactions WHERE tx_id = ?"
    ) {
        setString(1, id)
        executeUpdate()
    }

    /** Get first pending transaction */
    suspend fun first(): String? = db.serializable(
        "SELECT tx_id FROM pending_ebics_transactions LIMIT 1"
    ) {
        oneOrNull { it.getString(1) }
    }
}