/*
 * This file is part of LibEuFin.
 * Copyright (C) 2023-2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.nexus.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.parameters.groups.provideDelegate
import kotlinx.coroutines.delay
import tech.libeufin.common.*
import tech.libeufin.nexus.*
import tech.libeufin.nexus.db.PaymentBatch
import tech.libeufin.nexus.ebics.*
import tech.libeufin.nexus.iso20022.*
import java.time.Instant
import kotlin.time.toKotlinDuration

fun batchToPain001Msg(account: IbanAccountMetadata, batch: PaymentBatch): Pain001Msg {
    return Pain001Msg(
        messageId = batch.messageId,
        timestamp = batch.creationDate,
        debtor = account,
        sum = batch.sum,
        txs = batch.payments.map { payment ->
            val payto = payment.creditor
            Pain001Tx(
                creditor = IbanAccountMetadata(
                    iban = payto.iban.value,
                    bic = payto.bic,
                    name = payto.receiverName!!
                ),
                amount = payment.amount,
                subject = payment.subject,
                endToEndId = payment.endToEndId
            )
        }
    )
}

/** 
 * Submit an initiated payments [batch] using [client].
 * 
 * Parse creditor IBAN account metadata then perform an EBICS direct credit
 * 
 * Returns the orderID
 */
private suspend fun submitBatch(
    client: EbicsClient,
    batch: PaymentBatch
): String {
    val ebicsCfg = client.cfg.ebics
    val msg = batchToPain001Msg(ebicsCfg.account, batch)
    val xml = createPain001(
        msg = msg,
        dialect = ebicsCfg.dialect
    )
    return client.upload(
        ebicsCfg.dialect.directDebit(),
        xml
    )
}

/** Submit all pending initiated payments using [client] */
private suspend fun submitBatch(client: EbicsClient) {
    // Create batch if necessary
    client.db.initiated.batch(Instant.now(), randEbicsId())
    // Send submitable batches
    client.db.initiated.submittable().forEach { batch ->
        logger.debug("Submitting batch {}", batch.messageId)
        runCatching { submitBatch(client, batch) }.fold(
            onSuccess = { orderId -> 
                client.db.initiated.batchSubmissionSuccess(batch.id, Instant.now(), orderId)
                val transactions = batch.payments.map { it.endToEndId }.joinToString(",")
                logger.info("Batch ${batch.messageId} submitted as order $orderId: $transactions")
            },
            onFailure = { e ->
                client.db.initiated.batchSubmissionFailure(batch.id, Instant.now(), e.message)
                logger.error("Batch ${batch.messageId} submission failure: ${e.fmt()}")
                throw e
            }
        )
    }
}

class EbicsSubmit : CliktCommand() {
    override fun help(context: Context) = "Submits pending initiated payments found in the database"

    private val common by CommonOption()
    private val transient by transientOption()
    private val ebicsLog by ebicsLogOption()
    
    override fun run() = cliCmd(logger, common.log) {
        nexusConfig(common.config).withDb { db, cfg ->
            val (clientKeys, bankKeys) = expectFullKeys(cfg.ebics)
            val client = EbicsClient(
                cfg,
                httpClient(),
                db,
                EbicsLogger(ebicsLog),
                clientKeys,
                bankKeys
            )

            if (transient) {
                logger.info("Transient mode: submitting what found and returning.")
                submitBatch(client)
            } else {
                logger.debug("Running with a frequency of ${cfg.submit.frequencyRaw}")
                while (true) {
                    try {
                        submitBatch(client)
                    } catch (e: Exception) {
                        throw Exception("Failed to submit payments", e)
                    }
                    // TODO take submitBatch taken time in the delay
                    delay(cfg.submit.frequency.toKotlinDuration())
                }
            }
        }
    }
}