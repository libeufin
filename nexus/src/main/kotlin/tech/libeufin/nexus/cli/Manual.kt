/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.nexus.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.groups.provideDelegate
import com.github.ajalt.clikt.parameters.options.convert
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.*
import com.github.ajalt.mordant.terminal.*
import tech.libeufin.common.*
import tech.libeufin.nexus.*
import tech.libeufin.nexus.db.*
import tech.libeufin.nexus.ebics.*
import tech.libeufin.nexus.iso20022.*
import java.util.zip.*
import java.time.Instant
import java.io.*

class ExportCmt: CliktCommand("export") {
    override fun help(context: Context) = "Export pending batches as pain001 messages"

    private val common by CommonOption()
    private val out by argument().outputStream()

    override fun run() = cliCmd(logger, common.log) {
        nexusConfig(common.config).withDb { db, cfg ->
            // Create and get pending batches
            db.initiated.batch(Instant.now(), randEbicsId())
            val batches = db.initiated.submittable()

            var nbTx: Int = 0
            ZipOutputStream(BufferedOutputStream(out)).use { zip ->
                val ebicsCfg = cfg.ebics
                for (batch in batches) {
                    nbTx = batch.payments.size
                    val entry = ZipEntry("${batch.creationDate.toDateTimeFilePath()}-${batch.messageId}.xml")
                    zip.putNextEntry(entry)
                    val msg = batchToPain001Msg(ebicsCfg.account, batch)
                    
                    val xml = createPain001(
                        msg = msg,
                        dialect = ebicsCfg.dialect
                    )
                    zip.write(xml)
                    println("batch ${batch.messageId}:")
                    for (tx in batch.payments) {
                        println("tx ${tx.endToEndId} ${tx.amount} ${tx.creditor.iban} '${tx.creditor.receiverName}'")
                    }
                    println("")
                }
            }
            println("Exported ${batches.size} pain.001 files in $out")
        }
    } 
}

class ImportCmt: CliktCommand("import") {
    override fun help(context: Context) = "Import EBICS camt files"

    private val common by CommonOption()
    private val sources by argument().inputStream().multiple(required = true)

    override fun run() = cliCmd(logger, common.log) {
        nexusConfig(common.config).withDb { db, cfg ->
            for (source in sources) {
                var nbTx: Int = 0
                source.use { xml ->
                    nbTx += registerTxs(db, cfg, xml)
                }
                println("Imported $nbTx transactions from $source")
            }
        }
    } 
}

class StatusCmd: CliktCommand("status") {
    override fun help(context: Context) = "Change batches or transactions status"

    enum class Kind {
        batch,
        tx
    }

    private val common by CommonOption()
    private val kind by argument().enum<Kind>()
    private val id by argument()
    private val status by argument().enum<StatusUpdate>()
    private val msg by argument().optional()

    override fun run() = cliCmd(logger, common.log) {
        nexusConfig(common.config).withDb { db, cfg ->
            when (kind) {
                Kind.batch -> db.initiated.batchStatusUpdate(id, status, msg)
                Kind.tx -> db.initiated.txStatusUpdate(id, null, status, msg)
            }
        }
    } 
}

class ManualCmd : CliktCommand("manual") {
    init {
        subcommands(ExportCmt(), ImportCmt(), StatusCmd())
    }

    override fun help(context: Context) = "Manual management commands"

    override fun run() = Unit
}