/*
 * This file is part of LibEuFin.
 * Copyright (C) 2023 Stanisci and Dold.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

/**
 * This file collects all the CLI subcommands and runs
 * them.  The actual implementation of each subcommand is
 * kept in their respective files.
 */
package tech.libeufin.nexus

import io.ktor.server.application.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tech.libeufin.common.api.talerApi
import tech.libeufin.common.setupSecurityProperties
import tech.libeufin.nexus.api.revenueApi
import tech.libeufin.nexus.api.wireGatewayApi
import tech.libeufin.nexus.cli.LibeufinNexus
import tech.libeufin.nexus.db.Database
import com.github.ajalt.clikt.core.main

internal val logger: Logger = LoggerFactory.getLogger("libeufin-nexus")

/** Triple identifying one IBAN bank account */
data class IbanAccountMetadata(
    val iban: String,
    val bic: String?,
    val name: String
)

fun Application.nexusApi(db: Database, cfg: NexusConfig) = talerApi(logger) {
    wireGatewayApi(db, cfg)
    revenueApi(db, cfg)
}

fun main(args: Array<String>) {
    setupSecurityProperties()
    LibeufinNexus().main(args)
}