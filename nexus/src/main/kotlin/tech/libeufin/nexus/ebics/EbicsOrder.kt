/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */
package tech.libeufin.nexus.ebics

// We will support more dialect in the future

sealed class EbicsOrder(val schema: String) {
    data class V2_5(
        val type: String,
        val attribute: String
    ): EbicsOrder("H004")
    data class V3(
        val type: String,
        val service: String? = null,
        val scope: String? = null,
        val message: String? = null,
        val version: String? = null,
        val container: String? = null,
        val option: String? = null
    ): EbicsOrder("H005") {
        companion object {
            val WSS_PARAMS = V3(
                type = "BTD",
                service = "OTH",
                scope = "DE",
                message = "wssparam"
            )
            val HAC = V3(type = "HAC")
            val HKD = V3(type = "HKD")
            val HAA = V3(type = "HAA")
        }
    }

    fun description(): String = buildString {
        when (this@EbicsOrder) {
            is V2_5 -> {
                append(type)
                append('-')
                append(attribute)
            }
            is V3 -> {
                append(type)
                for (part in sequenceOf(service, scope, option, container)) {
                    if (part != null) {
                        append('-')
                        append(part)
                    }
                }
                if (message != null) {
                    append('-')
                    append(message)
                    if (version != null) {
                        append('.')
                        append(version)
                    }
                }
            }
        }
    }

    fun doc(): OrderDoc? {
        return when (this) {
            is V2_5 -> {
                when (this.type) {
                    "HAC" -> OrderDoc.acknowledgement
                    "Z01" -> OrderDoc.status
                    "Z52" -> OrderDoc.report
                    "Z53" -> OrderDoc.statement
                    "Z54" -> OrderDoc.notification
                    else -> null
                }
            }
            is V3 -> {
                when (this.type) {
                    "HAC" -> OrderDoc.acknowledgement
                    "BTD" -> when (this.message) {
                        "pain.002" -> OrderDoc.status
                        "camt.052" -> OrderDoc.report
                        "camt.053" -> OrderDoc.statement
                        "camt.054" -> OrderDoc.notification
                        else -> null
                    }
                    else -> null
                }
            }
        }
    }

    /** Check if two EBICS order match ignoring the message version */
    fun match(other: EbicsOrder): Boolean = when (this) {
        is V2_5 -> other is V2_5 
                    && type == other.type 
                    && attribute == other.attribute
        is V3 -> other is V3 
                    && type == other.type 
                    && service == other.service 
                    && scope == other.scope 
                    && message == other.message 
                    && container == other.container 
                    && option == other.option
    }
}

infix fun Collection<EbicsOrder>.matches(other: Collection<EbicsOrder>): List<EbicsOrder> 
    = this.filter { a -> other.any { b -> a.match(b) } }

enum class OrderDoc {
    /// EBICS acknowledgement - CustomerAcknowledgement HAC pain.002
    acknowledgement,
    /// Payment status - CustomerPaymentStatusReport pain.002
    status,
    /// Account intraday reports - BankToCustomerAccountReport camt.052
    report,
    /// Account statements - BankToCustomerStatement camt.053
    statement,
    /// Debit & credit notifications - BankToCustomerDebitCreditNotification camt.054
    notification;

    fun shortDescription(): String = when (this) {
        acknowledgement -> "EBICS acknowledgement"
        status -> "Payment status"
        report -> "Account intraday reports"
        statement -> "Account statements"
        notification -> "Debit & credit notifications"
    }

    fun fullDescription(): String = when (this) {
        acknowledgement -> "EBICS acknowledgement - CustomerAcknowledgement HAC pain.002"
        status -> "Payment status - CustomerPaymentStatusReport pain.002"
        report -> "Account intraday reports - BankToCustomerAccountReport camt.052"
        statement -> "Account statements - BankToCustomerStatement camt.053"
        notification -> "Debit & credit notifications - BankToCustomerDebitCreditNotification camt.054"
    }
}

enum class Dialect {
    postfinance,
    gls,
    maerki_baumann;

    fun downloadDoc(doc: OrderDoc, ebics2: Boolean): EbicsOrder {
        return when (this) {
            postfinance -> {
                if (ebics2) {
                    when (doc) {
                        OrderDoc.acknowledgement -> EbicsOrder.V2_5("HAC", "DZHNN")
                        OrderDoc.status -> EbicsOrder.V2_5("Z01", "DZHNN")
                        OrderDoc.report -> EbicsOrder.V2_5("Z52", "DZHNN")
                        OrderDoc.statement -> EbicsOrder.V2_5("Z53", "DZHNN")
                        OrderDoc.notification -> EbicsOrder.V2_5("Z54", "DZHNN")
                    }
                } else {
                    when (doc) {
                        OrderDoc.acknowledgement -> EbicsOrder.V3.HAC
                        OrderDoc.status -> EbicsOrder.V3("BTD", "PSR", "CH", "pain.002", "10", "ZIP")
                        OrderDoc.report -> EbicsOrder.V3("BTD", "STM", "CH", "camt.052", "08", "ZIP")
                        OrderDoc.statement -> EbicsOrder.V3("BTD", "EOP", "CH", "camt.053", "08", "ZIP")
                        OrderDoc.notification -> EbicsOrder.V3("BTD", "REP", "CH", "camt.054", "08", "ZIP")
                    }
                }
            }
            // TODO for GLS we might have to fetch the same kind of files from multiple orders
            gls -> when (doc) {
                OrderDoc.acknowledgement -> EbicsOrder.V3.HAC
                OrderDoc.status -> EbicsOrder.V3("BTD", "REP", "DE", "pain.002", null, "ZIP", "SCT")
                OrderDoc.report -> EbicsOrder.V3("BTD", "STM", "DE", "camt.052", null, "ZIP")
                OrderDoc.statement -> EbicsOrder.V3("BTD", "EOP", "DE", "camt.053", null, "ZIP")
                OrderDoc.notification -> EbicsOrder.V3("BTD", "STM", "DE", "camt.054", null, "ZIP", "SCI")
            }
            maerki_baumann -> throw IllegalArgumentException("Maerki Baumann does not have EBICS access")
        }
    }

    fun directDebit(): EbicsOrder {
        return when (this) {
            postfinance -> EbicsOrder.V3("BTU", "MCT", "CH", "pain.001", "09")
            gls -> EbicsOrder.V3("BTU", "SCT", null, "pain.001")
            maerki_baumann -> throw IllegalArgumentException("Maerki Baumann does not have EBICS access")
        }
    }

    /** All orders required for a dialect implementation to work */
    fun orders(): Set<EbicsOrder> = (
        // Administrative orders
        sequenceOf(EbicsOrder.V3.HAA, EbicsOrder.V3.HKD)
        // and documents orders
        + OrderDoc.entries.map { downloadDoc(it, false) }
    ).toSet()
}