/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.nexus

import tech.libeufin.common.*
import tech.libeufin.common.db.DatabaseConfig
import tech.libeufin.nexus.db.Database
import tech.libeufin.nexus.ebics.Dialect
import java.nio.file.Path
import java.time.Instant

val NEXUS_CONFIG_SOURCE = ConfigSource("libeufin", "libeufin-nexus", "libeufin-nexus")

data class NexusIngestConfig(
    val accountType: AccountType,
    val ignoreTransactionsBefore: Instant,
    val ignoreBouncesBefore: Instant
) {
    companion object {
        fun default(accountType: AccountType) 
            = NexusIngestConfig(accountType, Instant.MIN, Instant.MIN)
    }
}

class NexusFetchConfig(config: TalerConfig) {
    private val section = config.section("nexus-fetch")
    val frequency = section.duration("frequency").require()
    val frequencyRaw = section.string("frequency").require()
    val checkpointTime = section.time("checkpoint_time_of_day").require()
    val ignoreTransactionsBefore = section.date("ignore_transactions_before").default(Instant.MIN)
    val ignoreBouncesBefore = section.date("ignore_bounces_before").default(Instant.MIN)
}

class NexusSubmitConfig(config: TalerConfig) {
    private val section = config.section("nexus-submit")
    val frequency = section.duration("frequency").require()
    val frequencyRaw = section.string("frequency").require()
}

class NexusHostConfig(sect: TalerConfigSection) {
    /** The bank base URL */
    val baseUrl = sect.string("host_base_url").require()
    /** The bank EBICS host ID */
    val ebicsHostId = sect.string("host_id").require()
    /** EBICS user ID */
    val ebicsUserId = sect.string("user_id").require()
    /** EBICS partner ID */
    val ebicsPartnerId = sect.string("partner_id").require()
}

class NexusEbicsConfig( 
    sect: TalerConfigSection,
) {
    val host by lazy { NexusHostConfig(sect) }
    /** Bank account metadata */
    val account = IbanAccountMetadata(
        iban = sect.string("iban").require(),
        bic = sect.string("bic").require(),
        name = sect.string("name").require()
    )
    /** Bank account payto */
    val payto = IbanPayto.build(account.iban, account.bic, account.name)

    val dialect = sect.map("bank_dialect", "dialect", mapOf(
        "postfinance" to Dialect.postfinance,
        "gls" to Dialect.gls,
        "maerki_baumann" to Dialect.maerki_baumann,
    )).require()

    /** Path where we store the bank public keys */
    val bankPublicKeysPath = sect.path("bank_public_keys_file").require()
    /** Path where we store our private keys */
    val clientPrivateKeysPath = sect.path("client_private_keys_file").require()
}

class ApiConfig(section: TalerConfigSection) {
    val authMethod = section.requireAuthMethod()
}

/** Configuration for libeufin-nexus */
class NexusConfig internal constructor (val cfg: TalerConfig) {
    private val sect = cfg.section("nexus-ebics")

    val dbCfg by lazy { cfg.dbConfig() }
    val serverCfg by lazy {
        cfg.loadServerConfig("nexus-httpd")
    }

    /** The bank's currency */
    val currency = sect.string("currency").require()

    val accountType = sect.map("account_type", "account type", mapOf(
        "normal" to AccountType.normal,
        "exchange" to AccountType.exchange
    )).require()

    val fetch by lazy { NexusFetchConfig(cfg) }
    val submit by lazy { NexusSubmitConfig(cfg) }
    val ebics by lazy { NexusEbicsConfig(sect) }

    val ingest get() = NexusIngestConfig(
        accountType,
        fetch.ignoreTransactionsBefore,
        fetch.ignoreBouncesBefore
    )
   
    val wireGatewayApiCfg = cfg.section("nexus-httpd-wire-gateway-api").apiConf()
    val revenueApiCfg = cfg.section("nexus-httpd-revenue-api").apiConf()
}

fun NexusConfig.checkCurrency(amount: TalerAmount) {
    if (amount.currency != currency) throw badRequest(
        "Wrong currency: expected $currency got ${amount.currency}",
        TalerErrorCode.GENERIC_CURRENCY_MISMATCH
    )
}

private fun TalerConfigSection.requireAuthMethod(): AuthMethod {
    return mapLambda("auth_method", "auth method", mapOf(
        "none" to { AuthMethod.None },
        "bearer-token" to {
            logger.warn("Deprecated auth method option 'auth_method' used deprecated value 'bearer-token'")
            val token = string("auth_bearer_token").require()
            AuthMethod.Bearer(token)
        },
        "bearer" to {
            val token = string("token").require()
            AuthMethod.Bearer(token)
        },
        "basic" to {
            val username = string("username").require()
            val password = string("password").require()
            AuthMethod.Basic("$username:$password".encodeBase64())
        }
    )).require()
}

private fun TalerConfigSection.apiConf(): ApiConfig? {
    val enabled = boolean("enabled").require()
    return if (enabled) {
        return ApiConfig(this)
    } else {
        null
    }
}

sealed interface AuthMethod {
    data object None: AuthMethod
    data class Bearer(val token: String): AuthMethod
    data class Basic(val token: String): AuthMethod
}

enum class AccountType {
    normal,
    exchange
}

private fun TalerConfig.dbConfig(): DatabaseConfig {
    val sect = section("libeufin-nexusdb-postgres")
    val configOption = sect.string("config")
    return DatabaseConfig(
        dbConnStr = configOption.orNull() ?: section("nexus-postgres").string("config").orNull() ?: configOption.require(),
        sqlDir = sect.path("sql_dir").require()
    )
}

/** Load nexus config at [configPath] */
fun nexusConfig(configPath: Path?): NexusConfig {
    val config = NEXUS_CONFIG_SOURCE.fromFile(configPath)
    return NexusConfig(config)
}

/** Load nexus db config at [configPath] */
fun dbConfig(configPath: Path?): DatabaseConfig =
    NEXUS_CONFIG_SOURCE.fromFile(configPath).dbConfig()

/** Run [lambda] with access to a database conn pool */
suspend fun NexusConfig.withDb(lambda: suspend (Database, NexusConfig) -> Unit) {
    Database(dbCfg, currency).use { lambda(it, this) }
}