/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024-2025 Taler Systems S.A.

 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.

 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.nexus.api

import io.ktor.http.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import tech.libeufin.common.*
import tech.libeufin.common.api.intercept
import tech.libeufin.nexus.ApiConfig
import tech.libeufin.nexus.AuthMethod

/** Apply authentication api configuration for a route */
fun Route.auth(cfg: ApiConfig?, callback: Route.() -> Unit): Route =
    intercept("Auth", callback) {
        if (cfg?.authMethod != AuthMethod.None) {
            val header = this.request.headers[HttpHeaders.Authorization]
            val (expectedScheme, token) = when (val method = cfg?.authMethod) {
                is AuthMethod.Basic -> "Basic" to method.token
                is AuthMethod.Bearer -> "Bearer" to method.token
                else -> throw UnsupportedOperationException()
            }

            if (header == null) {
                this.response.header(HttpHeaders.WWWAuthenticate, expectedScheme)
                throw unauthorized(
                    "Authorization header not found",
                    TalerErrorCode.GENERIC_PARAMETER_MISSING
                )
            }
            val (scheme, content) = header.splitOnce(" ") ?: throw badRequest(
                "Authorization is invalid",
                TalerErrorCode.GENERIC_HTTP_HEADERS_MALFORMED
            )
            if (scheme == expectedScheme) {
                if (content != token) {
                    throw unauthorized("Unknown token", TalerErrorCode.GENERIC_TOKEN_UNKNOWN)
                }
            } else {
                throw unauthorized("Expected scheme $expectedScheme got '$scheme'")
            }
        }
    }

/** Apply conditional api configuration for a route */
fun Route.conditional(cfg: ApiConfig?, callback: Route.() -> Unit): Route =
    intercept("Conditional", callback) {
        if (cfg == null) {
            throw notImplemented()
        }
    }