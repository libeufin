/*
 * This file is part of LibEuFin.
 * Copyright (C) 2024 Taler Systems S.A.
 *
 * LibEuFin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3, or
 * (at your option) any later version.
 *
 * LibEuFin is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with LibEuFin; see the file COPYING.  If not, see
 * <http://www.gnu.org/licenses/>
 */

package tech.libeufin.nexus.test

import io.ktor.client.*
import tech.libeufin.common.*
import tech.libeufin.nexus.BankPublicKeysFile
import tech.libeufin.nexus.ClientPrivateKeysFile
import tech.libeufin.nexus.NexusEbicsConfig
import tech.libeufin.nexus.ebics.EbicsBTS
import tech.libeufin.nexus.ebics.EbicsOrder
import tech.libeufin.nexus.ebics.postBTS
import tech.libeufin.nexus.ebics.prepareUploadPayload
import tech.libeufin.nexus.logger

data class TxCheckResult(
    var concurrentFetchAndFetch: Boolean = false,
    var concurrentFetchAndSubmit: Boolean = false,
    var concurrentSubmitAndSubmit: Boolean = false,
    var idempotentClose: Boolean = false
)

/**
 * Test EBICS implementation's transactions semantic:
 * - Can two fetch transactions run concurrently ?
 * - Can a fetch & submit transactions run concurrently ?
 * - Can two submit transactions run concurrently ?
 * - Is closing a submit transaction idempotent
 */
suspend fun txCheck(
    client: HttpClient,
    cfg: NexusEbicsConfig,
    clientKeys: ClientPrivateKeysFile,
    bankKeys: BankPublicKeysFile,
    fetchOrder: EbicsOrder,
    submitOrder: EbicsOrder
): TxCheckResult {
    val result = TxCheckResult()
    val fetch = EbicsBTS(cfg, bankKeys, clientKeys, fetchOrder)
    val submit = EbicsBTS(cfg, bankKeys, clientKeys, submitOrder)

    suspend fun EbicsBTS.close(id: String, phase: String) {
        val xml = downloadReceipt(id, false)
        postBTS(client, xml, phase).okOrFail(phase)
    }

    val firstTxId = fetch.postBTS(client, fetch.downloadInitialization(null, null), "Init first fetch")
        .okOrFail("Init first fetch")
        .transactionID!!
    try {
        fetch.postBTS(client, fetch.downloadInitialization(null, null), "Init second fetch").ok()?.run {
            result.concurrentFetchAndFetch = true
            fetch.close(transactionID!!, "Init second fetch")
        }
        var paylod = prepareUploadPayload(cfg, clientKeys, bankKeys, ByteArray(2000000).rand())
        val submitId = submit.postBTS(client, submit.uploadInitialization(paylod), "Init first submit").ok()?.run {
                result.concurrentFetchAndSubmit = true
            transactionID!!
        }
        if (submitId != null) {
            submit.postBTS(client, submit.uploadTransfer(submitId, paylod, 1), "Submit upload").okOrFail("Submit first upload")
            submit.postBTS(client, submit.uploadInitialization(paylod), "Init second submit").ok()?.run {
                result.concurrentSubmitAndSubmit = true
            }
        }
    } finally {
        fetch.close(firstTxId, "Close first fetch")
    }

    try {
        fetch.close(firstTxId, "Close first fetch a second time")
        result.idempotentClose = true
    } catch (e: Exception) {
        logger.debug { e.fmt() }
    }

    return result
}